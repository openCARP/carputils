#!/usr/bin/env python3
"""
@package bin.build_pathway.py

The atrial-model-generation tool is an almost automatic workflow for the generation of patient-specific volumetric atrial models. It can be used to define single atrial chambers or bi-atrial geometries. It comprises six major sequential processing stages. First, starting from a tomographic scan (either CT or MRI), a convolutional neural network is employed to automatically segment the clinical image and label key anatomical structures, including heart blood pools and the left ventricular myocardium. The workflow then automatically augments the atrial blood pool with specific landmarks to identify the appendages, the ostia of the veins, and part of the vein tissue that will be removed in the next step to ensure the veins are open.

Based on these landmarks, an extrusion process is carried out to create the endocardial and epicardial layers, prescribing different wall thicknesses for the body of the atria and the veins. In this phase, the veins and valves are opened by avoiding extrusion at landmarks indicating removable tissue and at the interface between the atria and the ventricles. Next, a clustering algorithm is used to more precisely identify the orifices based on their position and distance from the appendages. Landmarks for the LSPV, LIPV, RSPV, RIPV, MV, IVC, SVC, CS, and TV are thus finally assigned to the corresponding openings.

Based on this information, a rule-based automatic labeling process is employed to define the major anatomical structures of the atria, including the CT, PM, ring of the FO, FO itself, and the lateral and (optionally) the anterior bands of the BB. These landmarks are finally used to define rule-based fiber bundles. As a last step, the workflow allows for the computation of the universal atrial coordinate system.

(c) 2024 Karli Gillette (karli.gillette@medunigraz.at) and Elena Zappon (elena.zappon@medunigraz.at)

"""

import os
import numpy

import carputils.tools
import carputils.cext.mshread as mshread
import carputils.cext.mshwrite as mshwrite

MESHTOOL = 'meshtool'
MGTAGREG = 'mgtagreg'


NODES_ORDER = ('insert', 'exit', 'insert2', 'exit2','con_insert', 'con_exit')

CONNECTIONS_DEFAULT_SPLIT = {'3':{'conn': ['con_insert', 'insert']},
                        '4':{'conn': ['con_insert', 'insert2']},
                        'split':{'conn': ['con_insert', 'con_exit']},
                        '5':{'conn': ['con_exit', 'exit']},
                        '6':{'conn': ['con_exit', 'exit2']}}

CONNECTIONS_DEFAULT = {'split':{'conn': ['insert', 'exit']}}

SEARCH_CONNECTION = {'insert','exit'}

def argument_parser():

    parser = carputils.tools.standard_parser()

    io_opts = parser.add_argument_group(':::::::::: input options :::::::::')
    io_opts.add_argument('--coords',
                        type=str,
                        required=True,
                        help=f'Pts file specifying entry points. File must be either 2, 3 or 4 points. '
                               f'If the file is 2 or 4 points, it has a specific order: {NODES_ORDER[0:4]}'
                               f'If the file is 3 points, a second file must be included with --coord_2,'
                               f'and it has the specific order {NODES_ORDER[0],NODES_ORDER[2],NODES_ORDER[4]}')

    io_opts.add_argument('--coords_2',
                         type=str,
                         required=False,
                          help=f'Pts file specifying a second series of entry points. File must be 3 points. '
                               f'It has a specific order: {NODES_ORDER[1],NODES_ORDER[3],NODES_ORDER[5]}')

    io_opts.add_argument('--path-type',
                        type=str,
                        default='geodasic',
                        choices=('geodasic','linear'),
                        help='Choose the type of the path computed between the insert and exit point. Default: geodasic')

    io_opts.add_argument('--mesh',
                         type=str,
                         required=True,
                         help='Filename of the mesh. ')
    
    io_opts.add_argument('--out-filename',
                         type=str,
                         required=False,
                         default='network',
                          help='Output filename.')

    mesh_opts = parser.add_argument_group(':::::::::: manual mesh options :::::::::')
    mesh_opts.add_argument('--tags',
                         type=str,
                         metavar='N,N,N,...',
                         help='Tags specifying the portion of the myocardium of the meshname in which to search for the entry and exit points. Tag list is comma seperated.')
    
    phys_opts = parser.add_argument_group(':::::::::: manual physical options :::::::::')
    phys_opts.add_argument('--conductance',
                         type=float,
                         required=False,
                         default=0.0006,
                         help='Conductance of the cable in ohm cm.')

    run_opts = parser.add_argument_group(':::::::::: run options :::::::::')
    run_opts.add_argument('--smooth',
                         type=int,
                         default=10,
                         help='Smoothing of the pathway when running in geodasic mode.')

    run_opts.add_argument('--rad-pt-displacement',
                         type=int,
                         default=10,
                         help='Displacement of the mean points when using a split configuration. [mm].')

    run_opts.add_argument('--network-dx',
                         default=500.0,
                         type=float,
                         # required=True,
                         help='Resolution of the network to be discretized. Please set this according to your mesh resolution.')

    search_opts = parser.add_argument_group(':::::::::: search options ::::::::::')

    search_opts.add_argument('--investigate-purk-points',
                        type=str,
                        required=False,
                        default='none',
                        help='Compute a set of Purkinje connections moving the entry or exit points. Available options are {SEARCH_CONNECTION}.')

    search_opts.add_argument('--radius-purk-points',
                        type=int,
                        default=1000,
                        help='Displacement of entry or exit point to build the set of Purkinje connections [micro m].')

    search_opts.add_argument('--sample-purk-points',
                        type=int,
                        default=20,
                        help='Factor to reduced the number of points sample to move the entry or exit points.')

    search_opts.add_argument('--tags-atria',
                         type=str,
                         metavar='N,N,N,...',
                         help='Tags specifying the atrial myocardium of the meshname. Tag list is comma seperated.')

    search_opts.add_argument('--source-tag-file',
                         type=str,
                         required=False,
                         help=f'Pts file specifying points representing the source points to create new tag area.'
                             f'The file must contained two points in the same entry-exit order.'
                             f'The points must be in cartesian coordinates.')



    return parser

def jobID(args):
    if args.ID is None:
        path_files = os.path.dirname(args.mesh)
        args.ID = path_files + f'/interatrial_pathways'
    return args.ID

def check_binary_mesh(job,meshname):

    bmesh=True
    if meshname[-1] == '.':
        meshname = meshname[:-1]

    print(f'Loading Mesh: {meshname}\n')
    for x in ('.belem', '.bpts','.blon'):
        if not os.path.exists(meshname + x):
            print(meshname + x)
            bmesh=False

        if not bmesh:
            print('Binary mesh does not exist. Converting..')
            cmd=[carputils.settings.execs.MESHTOOL,'convert',f'-imsh={meshname}.elem', f'-omsh={meshname}.belem']
            job.bash(cmd)

    return meshname

def parse_mt_query_file(filename, pt_type):

        print('\nReading Meshtool Query Output File')
        print(f'Filename: {filename}.out.txt')

        if pt_type == "xyz":
            filename = filename + '.txt'
        
        assert os.path.isfile(filename + '.out.txt'), 'File not found!'

        nodes_list = []
        with open(filename +'.out.txt', 'r') as fp:
            num = int(fp.readline().strip())

            print(f"Total number of localized points: {num}")

            for k in numpy.arange(num):
                nodes = [int(x) for x in fp.readline().strip().split(' ')]
                nodes_list.append(nodes)

        assert num == len(nodes_list), 'Number of localized nodes does not match the specified number of nodes in the header!'
    
        return nodes_list


def parse_meshtool_tag_file(filename):

        print('\nReading Modelgen Tag Region Output File')
        print(f'Filename: {filename}.tag')

        assert os.path.isfile(filename + '.tag'), 'File not found!'

        fp =  open(filename +'.tag', 'r')
        tags_list = fp.readlines()
        
        for x in range(numpy.size(tags_list)): 
            tags_list[x] = int(tags_list[x].replace('\n',''))

        fp.close()
        return tags_list

def displace_nodes(mesh_points, raw_nodes_list):

    print('\nPerforming a radial displacement of the connectors in the build split configuration!')

    insert_pt_cld = mesh_points[raw_nodes_list[4],:]
    exit_pt_cld = mesh_points[raw_nodes_list[5],:]

    min_dist = numpy.Inf
    ind_exit = exit_pt_cld[0]
    ind_insert = insert_pt_cld[0]
    for k,point in enumerate(exit_pt_cld):

        info= closest_node(point, insert_pt_cld)
        if info[0]< min_dist:
            min_dist = info[0]
            ind_insert = raw_nodes_list[4][info[1]]
            ind_exit = raw_nodes_list[5][k]

    raw_nodes_list[4] = [ind_insert]
    raw_nodes_list[5] = [ind_exit]

    return numpy.concatenate(raw_nodes_list)

def write_nodes(filename, nodes_list):
    with open(filename,'w') as f:
            f.write(f'{len(nodes_list)}\nextra\n')
            for node in nodes_list:
                f.write(f'{node}\n')

def closest_node(query_point, point_set, thr = 0.0):

        point_set   = numpy.asarray(point_set)

        dist_2      = numpy.sum((point_set - query_point) ** 2, axis=1)

        dist_2[numpy.where(dist_2 <= thr)] = numpy.Inf

        node = numpy.argmin(dist_2)
        pt = point_set[node]

        return dist_2[node], node, pt

def gen_conn_dict(mesh_points, nodes_list, base_filename):

        nodes = {}
        #Add the information to the dictionary!
        for k, node in enumerate(NODES_ORDER[:len(nodes_list)]):

            nodes[node] = {}

            ind = nodes_list[k]
            coord = mesh_points.take(int(ind), axis=0)

            vtx_file = base_filename + f'.{node}.vtx'
            with open(vtx_file, 'w') as f_out_single_pt_vtx:
                    f_out_single_pt_vtx.write(f'1\nextra\n{ind}\n')

            nodes[node]['ind']     = ind
            nodes[node]['coord']   = coord
            nodes[node]['vtx_file'] = vtx_file

        return nodes

def compute_paths(job, args, meshname, nodes, tags=None, build_split = True):

        cmd = ["mgspath",'--model-name', meshname,
               '--model-format','carp_bin',
               '--smooth', str(args.smooth)]

        if tags is not None:
            cmd+= ['--myo-tags']
            for tag in tags.split(','):
                cmd += [tag]

        cmd_path = []

        connections = (CONNECTIONS_DEFAULT,CONNECTIONS_DEFAULT_SPLIT)[build_split]
        for path_num, entry in connections.items():

            name_start = entry['conn'][0]
            name_end = entry['conn'][1]
            path_name = f'{name_start}_{name_end}'

            path_start  = nodes[name_start]['vtx_file']
            path_end    = nodes[name_end]['vtx_file']

            assert nodes[name_start]['ind'] != nodes[name_end]['ind'], \
                f'Nodes for path between insert and exit are localized to the same node. ' \
                f'Please try other settings. '

            if args.path_type == 'linear':
                connections[str(path_num)]['path'] = numpy.asarray([nodes[name_start]['coord'], nodes[name_end]['coord']])
            else:
                cmd_path += ['--path', path_name,
                       '{}'.format(path_start),
                       '{}'.format(path_end)]

        if args.path_type != 'linear':
            if not args.dry:
                job.bash(cmd + cmd_path)

            for path_num, entry in connections.items():

                name_start = entry['conn'][0]
                name_end = entry['conn'][1]
                path_name = f'{name_start}_{name_end}'

                path_filename = os.path.join(args.ID, 'path') + f'.{path_name}'

                job.mv(meshname + f'.path_{path_name}.pts',path_filename + '.pts')
                job.mv(meshname + f'.path_{path_name}.vtx',path_filename + '.vtx')

                connections[str(path_num)]['path'] = numpy.loadtxt(path_filename + '.pts', skiprows=1)

        if args.path_type == 'linear':
            return connections, ''
        else:
            return connections, path_filename

class Pathway:

    def __init__(self,args,job):

        if not os.path.exists(args.ID):
            job.mkdir(args.ID,parents=True)

        check_binary_mesh(job, args.mesh)
        mesh_points = mshread.read_points_bin(args.mesh + '.bpts')

        print(f'\nLoading coordinates: {args.coords}')
        coords = numpy.loadtxt(args.coords)
        if args.coords_2 is not None:
            print(f'Loading coordinates: {args.coords_2}')
            coords_2 = numpy.loadtxt(args.coords_2)

        if args.investigate_purk_points != 'none':
            assert os.path.isfile(args.source_tag_file + '.txt'), 'File not found!'

        #Check that the points file are in the correct configuration!
        if args.coords_2 is None:
            assert numpy.shape(coords)[0] in [2, 4], f'The coords file must have either 2 or 4 coordinates. Size: {len(coords)}'
        else:
            assert numpy.shape(coords)[0] in [3], f'The coords file must have 3 coordinates. Size: {len(coords)}'
            assert numpy.shape(coords_2)[0] in [3], f'The second coords file must have 3 coordinates. Size: {len(coords)}'

        assert numpy.shape(coords)[1] in [3,4], f'The coords must be in either pure UVC or UCC (No radial components). Size: {len(coords)}'
        if args.coords_2 is not None:
            assert numpy.shape(coords_2)[1] in [3,4], f'The second coords must be in either pure UVC or UCC (No radial components). Size: {len(coords)}'

        if args.investigate_purk_points != 'none':
            assert args.investigate_purk_points in SEARCH_CONNECTION, f'The search for the Purkinje points should be {SEARCH_CONNECTION}.'

        pt_type = ('ucc','xyz')[numpy.shape(coords)[1] == 3]

        print(f'Number of coords: {len(coords)}')

        #Check which configuration we will build: Either split or not
        build_split = (True, False)[len(coords) == 2]
        print(f'Building split configuration: {build_split}\n')

        if build_split:
            if args.coords_2 is None:
                mean_insert = numpy.mean([coords[0,:],coords[2,:]],axis=0)
                mean_exit = numpy.mean([coords[1, :], coords[3, :]],axis=0)

                coords = numpy.vstack((coords, mean_insert, mean_exit))

                rad_append = numpy.reshape([0, 0, 0, 0, args.rad_pt_displacement, args.rad_pt_displacement], (6,1))
            else:
                coords = numpy.vstack((coords[0,:],coords_2[0,:],coords[1,:],coords_2[1,:],coords[2,:],coords_2[2,:]))
                rad_append = numpy.reshape([0, 0, 0, 0, 0, 0], (6,1))
        else:
            if args.investigate_purk_points in SEARCH_CONNECTION:
                if args.investigate_purk_points == 'insert':
                    rad_append = numpy.reshape([args.radius_purk_points, 0], (2,1))
                else:
                    rad_append = numpy.reshape([0, args.radius_purk_points], (2,1))
            else:
                rad_append = numpy.reshape([0, 0], (2,1))

        coords = numpy.hstack((coords,rad_append))

        node_filename = os.path.join(args.ID, 'base_coords')
        print(f'Writing node search file: {node_filename}.txt')
        with open(node_filename + '.txt','w') as f:
            f.write(f'{len(coords)}\n')
            for pt in coords:
                if pt_type == 'xyz':
                    pt[3]*=10e2
                    f.write('{} {} {} {} 0\n'.format(*pt))
                elif pt_type == 'ucc':
                    pt[4]*=10e2
                    f.write('{} {} {} {} {} 0\n'.format(*pt))
        
        search_tool = ('idxlist','idxlist_ucc')[pt_type=='ucc']
        
        # if not os.path.exists(args.mesh + '.surf'):

        #     extract_surface_cmd = [MESHTOOL,'extract','surface',
        #                         '-msh', args.mesh,
        #                         '-surf', args.mesh]
        
        #     print(' '.join(extract_surface_cmd))
                   
        #     if not args.dry:
        #         os.system(' '.join(extract_surface_cmd))

        if args.investigate_purk_points != 'none':
            if not os.path.exists(args.mesh + '_submesh'):
                extract_submesh_cmd = [MESHTOOL,'extract','mesh',
                                    '-msh', args.mesh,
                                    '-tags', args.tags_atria,
                                    '-submsh', args.mesh + '_submesh']
                print(' '.join(extract_submesh_cmd))

                if not args.dry:
                    os.system(' '.join(extract_submesh_cmd))
            
            submesh_map = numpy.fromfile(args.mesh + '_submesh.nod',dtype=numpy.long)  
            submesh_map_elem = numpy.fromfile(args.mesh + '_submesh.eidx',dtype=numpy.long) 
            mesh_tag_points_bin = mshread.read_element_tags_bin(args.mesh + '.belem')               
        else:
            print(f"The {args.mesh + '.submesh'} already exists!")
        
        if (search_tool == 'idxlist'):
            locate_cmd = [MESHTOOL, 'query', search_tool,
                        '-msh', args.mesh,
                        #'-surf', args.mesh,
                        '-tags',args.tags,
                        '-coord', node_filename + '.txt']
        else:
            locate_cmd = [MESHTOOL, 'query', search_tool,
                        '-msh', args.mesh,
                        '-coord', node_filename + '.txt',
                        '-vtx', str(1)]
        
        print(' '.join(locate_cmd))
        if not args.dry:
            os.system(' '.join(locate_cmd))

        nodes_list = parse_mt_query_file(node_filename,pt_type)
        
        if args.investigate_purk_points != 'none':
            locate_source_cmd = [MESHTOOL, 'query', 'idxlist',
                            '-msh', args.mesh + '_submesh',
                            '-tags', args.tags_atria,
                            '-coord', args.source_tag_file + '.txt',
                            '-vtx', str(1)]
            
            print(' '.join(locate_source_cmd))

            if not args.dry:
                os.system(' '.join(locate_source_cmd))

            nodes_source_list = parse_mt_query_file(args.source_tag_file,'xyz')

        
        if build_split:
            if args.coords_2 is None:
                nodes_lists = displace_nodes(mesh_points, nodes_list)
            else:
                nodes_lists = numpy.concatenate(nodes_list)
        else:
            if args.investigate_purk_points in SEARCH_CONNECTION:
                nodes_lists = []
                if args.investigate_purk_points == 'insert':
                    search_points = mesh_points[nodes_list[0],:][::args.sample_purk_points]
                    nodes_list[0] = nodes_list[0][::args.sample_purk_points]
                else:
                    search_points = mesh_points[nodes_list[1],:][::args.sample_purk_points]
                    nodes_list[1] = nodes_list[1][::args.sample_purk_points]

                with open(os.path.join(args.ID,'search_points.pts'),'w') as fo:
                    n_points = len(search_points)
                    print(f"Total number of {args.investigate_purk_points} points: {n_points}")
                    fo.write('{}\n'.format(n_points))

                    for k,point in enumerate(search_points):
                        prec_point= numpy.around(point, 1)
                        fo.write('{} {} {}\n'.format(*prec_point))
                        if args.investigate_purk_points == 'insert':
                            nodes_lists.append([nodes_list[0][k],nodes_list[1][0]])
                        else:
                            nodes_lists.append([nodes_list[0][0],nodes_list[1][k]])                        
            else:
                nodes_lists = numpy.concatenate(nodes_list)
        
        for k,list in enumerate(nodes_lists):
            if args.investigate_purk_points in SEARCH_CONNECTION:
                nodes_list = list
            else:
                nodes_list = nodes_lists
            
            print(f'Localized Nodes: {nodes_list}')
            write_nodes(node_filename + '.localized.vtx', nodes_list)

            vtx_filename = os.path.join(args.ID,'node')
            nodes = gen_conn_dict(mesh_points, nodes_list, vtx_filename)

            #We need to split the cable to allow for the proper structure!!
            connections, path_filename = compute_paths(job, args, args.mesh, nodes, tags = args.tags, build_split = build_split)

            path_info = connections['split']['path']
            if args.path_type == 'geodasic':
                split_cable = path_info[:len(path_info) // 2,:]
                middle_point = split_cable[len(split_cable)-1,:]
                Cab1 = path_info[(len(path_info) // 2)-1:,:]
                Cab2 = split_cable[::-1]

            elif args.path_type == 'linear':
                middle_point = numpy.mean(path_info,axis=0)
                Cab1 = [middle_point, path_info[1]]
                Cab2 = [middle_point, path_info[0]]

            else:
                raise IOError(f'The path type of {args.path_type} is not supported!')

            _,_,pt = closest_node(middle_point,mesh_points,thr=5.0)

            Cab0 = []
            Cab0.append(pt)
            Cab0.append(middle_point)

            self.cab1 = Cab1
            self.cab2 = Cab2
            self.cab0 = Cab0

            network_filename = os.path.join(args.ID, args.out_filename + '_' + str(k))

            if build_split:
                self.write_split_purkinje_file(connections, network_filename, args.conductance)
            else:
                self.write_single_purkinje_file(network_filename, args.conductance)

            #Tag the new region
            if args.investigate_purk_points != 'none':

                vtx_pkje_submesh = numpy.zeros((2,1), dtype=int)

                #Search vtx index on the submesh of the entry and exit point
                for ii in range(numpy.size(submesh_map)):
                    if submesh_map[ii] == nodes_list[0]:
                        vtx_pkje_submesh[0] = int(ii)
                    if submesh_map[ii] == nodes_list[1]:
                        vtx_pkje_submesh[1] = int(ii)
                
                # Estract first tag 
                tag_entry_cmd = [MGTAGREG, '-np 1',
                            '--model-name', args.mesh + '_submesh',
                            '--dijkstra-region', 'entry_tag', 
                            str(nodes_source_list[1][0]), 
                            str(vtx_pkje_submesh[0][0]),
                            str(1.5), str(56)]
                
                print(' '.join(tag_entry_cmd))  

                if not args.dry:
                    os.system(' '.join(tag_entry_cmd))

                # Estract second tag
                tag_exit_cmd = [MGTAGREG, '-np 1',
                            '--model-name', args.mesh + '_submesh',
                            '--dijkstra-region', 'exit_tag', 
                            str(nodes_source_list[0][0]), 
                            str(vtx_pkje_submesh[1][0]),
                            str(1.5), str(23)]
                    

                print(' '.join(tag_exit_cmd))  

                if not args.dry:
                    os.system(' '.join(tag_exit_cmd))

                entry_tags_list = parse_meshtool_tag_file(args.mesh + '_submesh.region_entry_tag')
                idx_entry_new_tags = numpy.squeeze(numpy.nonzero(entry_tags_list))
                exit_tags_list = parse_meshtool_tag_file(args.mesh + '_submesh.region_exit_tag')
                idx_exit_new_tags = numpy.squeeze(numpy.nonzero(exit_tags_list))
                
                for ii in range(numpy.size(idx_entry_new_tags)):
                    mesh_tag_points_bin[submesh_map_elem[idx_entry_new_tags[ii]]] = entry_tags_list[idx_entry_new_tags[ii]]
                
                for ii in range(numpy.size(idx_exit_new_tags)):
                    mesh_tag_points_bin[submesh_map_elem[idx_exit_new_tags[ii]]] = exit_tags_list[idx_exit_new_tags[ii]]
                
                basename_mesh = os.path.basename(args.mesh)
                
                new_tag_filename = os.path.join(args.ID, basename_mesh + '.tags_pkje_' + str(k) +'.btag')
                #mshwrite.write_element_btags(new_tag_filename, numpy.array(mesh_tag_points_bin, dtype=numpy.intc))
                mesh_tag_points_bin = numpy.array(mesh_tag_points_bin, dtype=numpy.intc)
                mesh_tag_points_bin.tofile(new_tag_filename)
                
                new_tag_filename = os.path.join(args.ID, basename_mesh + '.tags_pkje_' + str(k) +'.tag')
                with open(new_tag_filename,'w') as fo:
                    for ii in range(numpy.size(mesh_tag_points_bin)):
                        fo.write('{}\n'.format(mesh_tag_points_bin[ii]))
                
            if args.investigate_purk_points not in SEARCH_CONNECTION:
                break
                


    def write_split_purkinje_file(self, connections, filename, conductance):

        print(f'Writing purkinje file: {filename}')
        with open(filename + '.pkje','w') as fo:
            fo.write('7\n')
            fo.write(f'Cable 0\n')
            fo.write('-1 -1\n')
            fo.write('2 1\n')
            fo.write(f'{len(self.cab0)}\n' )
            fo.write('7.0\n' )
            fo.write('100.0\n' )
            fo.write(f'{conductance}\n')
            for entry in self.cab0:
                prec_point= numpy.around(entry, 1)
                fo.write('{} {} {}\n'.format(*prec_point))

            fo.write(f'Cable 1\n')
            fo.write('0 -1\n')
            fo.write('5 6\n')
            fo.write(f'{len(self.cab1)}\n' )
            fo.write('7.0\n' )
            fo.write('100.0\n' )
            fo.write(f'{conductance}\n' )
            for entry in self.cab1:
                prec_point= numpy.around(entry, 1)
                fo.write('{} {} {}\n'.format(*prec_point))

            fo.write(f'Cable 2\n')
            fo.write('0 -1\n')
            fo.write('3 4\n')
            fo.write(f'{len(self.cab2)}\n' )
            fo.write('7.0\n' )
            fo.write('100.0\n')
            fo.write(f'{conductance}\n' )
            for entry in self.cab2:
                prec_point= numpy.around(entry, 1)
                fo.write('{} {} {}\n'.format(*prec_point))

            fo.write(f'Cable 3\n')
            fo.write('2 -1\n')
            fo.write('-1 -1\n')
            fo.write(f'{len(connections["3"]["path"])}\n' )
            fo.write('7.0\n' )
            fo.write('100.0\n' )
            fo.write(f'{conductance}\n')
            for entry in connections['3']['path']:
                prec_point= numpy.around(entry, 1)
                fo.write('{} {} {}\n'.format(*prec_point))

            fo.write(f'Cable 4\n')
            fo.write('2 -1\n')
            fo.write('-1 -1\n')
            fo.write(f'{len(connections["4"]["path"])}\n')
            fo.write('7.0\n')
            fo.write('100.0\n')
            fo.write(f'{conductance}\n')
            for entry in connections['4']['path']:
                prec_point = numpy.around(entry, 1)
                fo.write('{} {} {}\n'.format(*prec_point))

            fo.write(f'Cable 5\n')
            fo.write('1 -1\n')
            fo.write('-1 -1\n')
            fo.write(f'{len(connections["5"]["path"])}\n')
            fo.write('7.0\n')
            fo.write('100.0\n')
            fo.write(f'{conductance}\n')
            for entry in connections['5']['path']:
                prec_point = numpy.around(entry, 1)
                fo.write('{} {} {}\n'.format(*prec_point))

            fo.write(f'Cable 6\n')
            fo.write('1 -1\n')
            fo.write('-1 -1\n')
            fo.write(f'{len(connections["6"]["path"])}\n')
            fo.write('7.0\n')
            fo.write('100.0\n')
            fo.write(f'{conductance}\n')
            for entry in connections['6']['path']:
                prec_point = numpy.around(entry, 1)
                fo.write('{} {} {}\n'.format(*prec_point))

    def write_single_purkinje_file(self, filename, conductance):

        print(f'Writing purkinje file: {filename}')
        with open(filename + '.pkje','w') as fo:
                fo.write('3\n')
                fo.write(f'Cable 0\n')
                fo.write('-1 -1\n')
                fo.write('2 1\n')
                fo.write(f'{len(self.cab0)}\n' )
                fo.write('7.0\n' )
                fo.write('100.0\n' )
                fo.write(f'{conductance}\n' )
                for entry in self.cab0:
                    prec_point= numpy.around(entry, 1)
                    fo.write('{} {} {}\n'.format(*prec_point))

                fo.write(f'Cable 1\n')
                fo.write('0 -1\n')
                fo.write('-1 -1\n')
                fo.write(f'{len(self.cab1)}\n' )
                fo.write('7.0\n' )
                fo.write('100.0\n' )
                fo.write(f'{conductance}\n' )
                for entry in self.cab1:
                    prec_point= numpy.around(entry, 1)
                    fo.write('{} {} {}\n'.format(*prec_point))

                fo.write(f'Cable 2\n')
                fo.write('0 -1\n')
                fo.write('-1 -1\n')
                fo.write(f'{len(self.cab2)}\n' )
                fo.write('7.0\n' )
                fo.write('100.0\n' )
                fo.write(f'{conductance}\n' )
                for entry in self.cab2:
                    prec_point= numpy.around(entry, 1)
                    fo.write('{} {} {}\n'.format(*prec_point))


@carputils.tools.carpexample(argument_parser, jobID )
def main(args,job):
    Pathway(args,job)

if __name__ == '__main__':
    main()
