#!/usr/bin/env python3

"""
@package bin.carphelp
openCARP parameters helper

This script allows you to find openCARP parameters containing a given
search pattern, and to get their documentation.
Use "+" to concatenate multiple search patterns.

Usage:
    > carphelp pattern pattern ... [--no-col]

Example:
    > carphelp imp_region stim+type
"""

import os
import sys
import subprocess
import carputils.cml
import carputils.settings.paths

from carputils import settings

GRCOL = '\33[1;32m'
YLCOL = '\33[1;33m'
BLCOL = '\33[1;34m'
NOCOL = '\33[0m'


KEYWORDS = ('type', 'min', 'max', 'default',
            'units', 'menu', 'ext', 'Depends on',
            'Changes the allocation of', 'Influences the value of',
            'Changes the default value of')

DTYPES = ('Short', 'Int', 'Size_t', 'Float', 'Double',
          'WFile', 'RFile', 'String', 'Flag')

def highlight_keywords(string):
    """
    For each keyword in the predefined list KEYWORDS,
    find the pattern keyword+':' in the string and highlight keyword.

    Args:
        string : str
            string in which keywords should be highlighted
    Returns:
        string : str
            the string with keywords highlighted
        totcount : int
            the number of keywords found in the string
    """
    totcount = 0
    for keyword in KEYWORDS:
        kwcount = string.count(keyword+':')
        if kwcount > 0:
            string = string.replace('{}:'.format(keyword), YLCOL+'{}'.format(keyword)+NOCOL+':')
        totcount += kwcount
    return string, totcount

def highlight_types(string, prefix='', suffix=''):
    """
    For each element dtype in DTYPES, highlight dtype if prefix+dtype+suffix
    is contained in string.

    Returns:
        string : str
            string in which matching patterns are highlighted
        totcount : str
            number of matching patterns
    """
    totcount = 0
    for dtype in DTYPES:
        surrounded_dtype = prefix+dtype+suffix
        dtcount = string.count(surrounded_dtype)
        if dtcount > 0:
            string = string.replace('{}'.format(surrounded_dtype),
                                    prefix+BLCOL+'{}'.format(dtype)+NOCOL+suffix)
        totcount += dtcount
    return string, totcount


def build_helpcmd(carpexec, args):
    grepcmd = ' '.join(['| grep {}'.format(arg) for arg in args])
    cmd = '({} +Help {}) || true'.format(carpexec, grepcmd)
    return cmd


def main(carpexec, arg, colors=True):
    """
    Prints documentation about matching openCARP parameters.

    Args:
        carpexec: str
            openCARP executable
        arg: str
            search patterns for parameters
        colors: bool
            if True, highlighting is used on the output
    """
    # split search patterns and remove empty strings from list
    args = [i for i in arg.split('+') if i]

    # run the Help command and capture output
    cmd = '({} +Help) || true'.format(carpexec)
    output = subprocess.check_output(cmd, shell=True)

    rawcarpargs = list()
    for line in map(lambda s: s.strip(), output.decode().split('\n')):
        name = line.split(' ')[0].strip()
        if len(name) > 0 and all([arg in name for arg in args]):
            rawcarpargs.append(name)

    # clean raw openCARP arguments
    carpargs = []
    for carparg in rawcarpargs:
        if len(carparg) > 0:
            # remove surrounding quotes
            if carparg[0] == '\'' and carparg[-1] == '\'':
                carparg = carparg[1:-1]

            # remove leading minus sign
            if carparg[0] == '-':
                carparg = carparg[1:]

            # replace '[Int]' by '[0]'
            carparg = carparg.replace('[Int]', '[0]')
            # replace '[size_t]' by '[0]'
            carparg = carparg.replace('[size_t]', '[0]')
            if carparg != carpexec:
                carpargs.append(carparg)

    carpargs = [i for i in carpargs if i+'[0]' not in carpargs]

    # run carp +Help for each argument
    for carparg in carpargs:
        cmd = '({} +Help {}) || true'.format(carpexec, carparg)
        helptext = subprocess.check_output(cmd, shell=True)
        helplines = [i for i in helptext.decode().split('\n') if i]
        # if colors are used, highlight predefined keywords
        if colors:
            helplines[0] = GRCOL+helplines[0]+NOCOL
            for i, line in enumerate(helplines[1:]):
                line, nrep = highlight_keywords(line)
                if nrep > 0:
                    line, _ = highlight_types(line)
                else:
                    line, _ = highlight_types(line, '(', ')')
                helplines[i+1] = line
        print('\n'+'\n'.join(helplines))


def print_help():
    print('usage "{} pattern pattern ... [--no-col]"'.format(os.path.basename(sys.argv[0])))
    print('  use "+" to concatenate multiple search patterns')
    print('\nexample "{} imp_region stim+type"\n'.format(os.path.basename(sys.argv[0])))


if __name__ == '__main__':

    settings.cli = {}
    carpexec = settings.execs['CARP']
    if carpexec is None:
        print('no CARP executable found!')
        exit(1)

    print('executable "{}"'.format(carpexec))
    if len(sys.argv) <= 1:
        print_help()
        exit(1)

    args = sys.argv[1:]
    colors = True
    if '--no-col' in args:
        colors = False
        args.remove('--no-col')

    if len(args) > 0:
        for arg in args:
            main(carpexec, arg, colors)
    else:
        print_help()

