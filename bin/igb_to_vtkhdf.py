"""
@package bin.igb_to_vtkhdf
Convert igb files to VTKHDF format.
"""

import os.path
import numpy as np
import re
import argparse

try:
    import h5py
except ImportError as e:
    raise Exception("""Couldn't import optional dependency h5py. Consider installing carputils with all optional dependencies with:
                    $ pip install carputils[full]
                    or install h5py with:
                    $ pip install h5py""")


def arg_parser():
    """
    Generate a standard argument parser for collection of common options.

    Returns :
        argparse.Namespace: Parsed arguments.
    """
    parser = argparse.ArgumentParser(description='Analysis of AP recordings')
    parser.add_argument('--igb_path',
                        type=str,
                        required=True,
                        help='path to igb file (with file extension)')
    parser.add_argument('--pts_path',
                        type=str,
                        required=True,
                        help='path to pts file (with file extension)')
    parser.add_argument('--elem_path',
                        type=str,
                        required=True,
                        help='path to elem file (with file extension)')
    parser.add_argument('--cell_type',
                        type=int,
                        required=True,
                        help='cell type according to VTK nomenclature. '\
                             'Examples: 5 = triangle, 9 = quad, 10 = tetra, 12 = hexaedron')
    parser.add_argument('--output_path',
                        type=str,
                        default="converted_igb.vtkhdf",
                        help='path to output file. Extension ".vtkhdf" will be appended if not provided. '\
                             'Default: converted_igb.vtkhdf')
    parser.add_argument('--stride',
                        type=int,
                        default=1,
                        help='define stride if you want to skip time steps (e.g. save only every 10th)')
    parser.add_argument('--max_memory',
                        type=float,
                        default=4,
                        help='maximum memory in GB you want to use for the conversion')
    return parser.parse_args()


def main(args):
    """
    Execute functions and create vtkhdf file.

    Args:
        args (argparse.Namespace): Contains parsed arguments as returned by the function arg_parser.
    """
    # check arguments
    check_args(args)

    # read mesh
    geometry = read_pts(args.pts_path)
    topology = read_elem(args.elem_path)

    # get number of cells, points, connections and points per cell
    num_geom = {"num_cells": topology.shape[0],
                "num_verts": geometry.shape[0],
                "num_conns": topology.size,
                "num_verts_per_cell": topology.shape[1]}

    # create VTKHDF file
    create_vtkhdf(args.output_path, geometry, topology, num_geom, args.cell_type)

    # append to VTKHDF file
    # initialize next time step
    ts_next = 0
    # iterate over scalar data in chunks
    for chunk_data in read_igb_in_chunks(args.igb_path, args.max_memory):
        ts_next = write_vtkhdf(args.output_path, chunk_data, num_geom, ts_next, args.stride)

    print(f"\nFile written to '{args.output_path}'.\n")


def check_args(args):
    """
    Check given arguments in general.

    Args:
        args (argparse.Namespace): Contains parsed arguments.

    Raises:
        FileNotFoundError: If file to be read at the given path does not exist.
        SystemExit: If file to be written already exists and can't be overwritten.
    """
    # check if .igb, .pts and .elem files exist
    for file in [args.igb_path, args.pts_path, args.elem_path]:
        if not os.path.exists(file):
            raise FileNotFoundError(f"The file '{file}' does not exist.")

    # check if output path has correct file ending
    if not args.output_path.endswith(".vtkhdf"):
        args.output_path = f"{args.output_path}.vtkhdf"

    # check if output file already exists
    if os.path.exists(args.output_path):
        overwrite = []
        # ask to overwrite file
        while not overwrite in ["y", "n"]:
            overwrite = input(f"\nPath to output file '{args.output_path}' already exists.\nOverwrite file? [y/n] ")
            if not overwrite in ["y", "n"]:
                print("Please answer with y or n.")
            if overwrite == "n":
                raise SystemExit("Aborting.")
                
            elif overwrite == "y":
                # delete file
                os.remove(args.output_path)
                print(f"Overwriting file '{args.output_path}'.")


def read_pts(file_path:str, skip_invalid:bool=True)->np.ndarray:
    """
    Read in points of the mesh.

    Args:
        file_path (str): Contains file path of the mesh.
        skip_invalid (bool, optional): Skips invalid files. Defaults to True.

    Raises:
        ValueError: If file contains invalid lines. 

    Returns:
        np.ndarray: Contains array with all points of the mesh.
    """
    # read geometry points from .pts file, optionally skipping or logging invalid lines
    points = []
    invalid_lines = []
    with open(file_path, 'r') as f:
        for i, line in enumerate(f, start=1):
            line = line.strip()
            if line.startswith('#') or not line:
                continue  # skip comments or empty lines

            # split line into parts (coordinates of points)
            parts = line.split()
            if len(parts) == 3:
                try:
                    points.append([float(parts[0]), float(parts[1]), float(parts[2])])
                except ValueError:
                    invalid_lines.append((i, line))
            else:
                invalid_lines.append((i, line))

    if invalid_lines:
        print(f"Encountered {len(invalid_lines)} invalid line(s) in {file_path}.")
        for i, line in invalid_lines:
            print(f"Line {i}: {line}")
        if not skip_invalid:
            raise ValueError("File contains invalid lines. Check the output for details.")

    return np.array(points, dtype=np.float32)


def read_elem(file_path:str)->np.ndarray:
    """
    Read in elements of the mesh.

    Args:
        file_path (str): Contains file path of the mesh.

    Raises:
        ValueError: If elements are not triangles or tetrahedra.

    Returns:
        np.ndarray: Contains array with all elements of the mesh.
    """
    # read element topology from .elem file, logging or skipping invalid lines
    elems = []
    invalid_lines = []

    with open(file_path, 'r') as f:
        for i, line in enumerate(f, start=1):
            line = line.strip()
            if line.startswith('#') or not line:
                continue  # skip comments or empty lines

            parts = line.split()
            if len(parts) < 3:
                continue
            try:
                # first value is the type of object, last the element tag
                if parts[0].startswith('Tr'):
                    indices = list(map(int, parts[1:4]))  # first 3 values are point indices
                    elems.append(indices)
                elif parts[0].startswith('Tt'):
                    indices = list(map(int, parts[1:5]))  # first 4 values are point indices
                    elems.append(indices)
                else:
                    raise ValueError("Only triangles and tetrahedra are supported at the moment. "
                                     "Check lines 140ff in code to add other cell types.")
            except ValueError:
                invalid_lines.append((i, line))

    if invalid_lines:
        print(f"Encountered {len(invalid_lines)} invalid line(s) in {file_path}.")
        for i, line in invalid_lines:
            print(f"Line {i}: {line}")

    return np.array(elems, dtype=np.int32)


def read_igb_in_chunks(file_path:str, max_memory_gigabytes:float):
    """
    Read in igb data as chunks.

    Args:
        file_path (str): Contains file path of the mesh.
        max_memory_gigabytes (float): Contains maximum memory in gigabytes.

    Yields:
        np.ndarray: Contains data seperated in chunks.
    """
    with open(file_path, 'rb') as f:
        # read the header first
        header = f.read(1024)
        header_txt = header.decode('ascii')
        pattern = r'(\w+):([\w\.]+)'

        # find all matches in the header to extract metadata
        matches = re.findall(pattern, header_txt)

        # convert to dictionary for easy access
        data_dict = {key: value for key, value in matches}

        # get the number of nodes and time steps from the header
        num_nodes = int(data_dict["x"])  # number of nodes
        num_timesteps = int(data_dict["t"])  # number of time steps

        # read the data in chunks
        data_offset = 1024  # offset where the actual data starts (after header)

        # convert gigabytes to bytes
        max_memory_bytes = max_memory_gigabytes * 1024 * 1024 * 1024

        # calculate how many time steps can fit in the given memory
        bytes_per_timestep = num_nodes * 4  # each entry is 4 bytes (float32)
        timesteps_per_chunk = max(1, int(max_memory_bytes / bytes_per_timestep))

        for start_timestep in range(0, num_timesteps, timesteps_per_chunk):
            # calculate the number of time steps to read in this chunk
            end_timestep = min(start_timestep + timesteps_per_chunk, num_timesteps)
            num_timesteps_in_chunk = end_timestep - start_timestep

            # seek to the position in the file to start reading
            f.seek(data_offset + start_timestep * bytes_per_timestep)

            # read the chunk of data (for 'num_timesteps_in_chunk' time steps)
            chunk_data = np.frombuffer(f.read(num_timesteps_in_chunk * bytes_per_timestep), dtype=np.float32)

            # reshape the chunk data to (num_timesteps_in_chunk, num_nodes)
            chunk_data = chunk_data.reshape(num_timesteps_in_chunk, num_nodes)

            # yield the chunk data (to be processed or appended to the VTKHDF file)
            yield chunk_data


def create_vtkhdf(file_path:str, geometry:str, topology:str, num_geom:dict, cell_type:int):
    """
    Create vtkhdf file.

    Args:
        file_path (str): Contains file path of the mesh.
        geometry (str): Contains specified geometry of the mesh.
        topology (str): Contains specified topology of the mesh.
        num_geom (dict): Contains number of cells, points, connections and points per cell.
        cell_type (int): Contains specified cell type.
    """
    # get number of cells, points, connections and points per cell
    num_cells = num_geom["num_cells"]
    num_verts = num_geom["num_verts"]
    num_conns = num_geom["num_conns"]
    num_verts_per_cell = num_geom["num_verts_per_cell"]

    # create file
    f = h5py.File(file_path, "w")

    # generate top level group (root) and populate its attributes
    root = f.create_group("VTKHDF")
    ascii_type = h5py.string_dtype('ascii', 16)
    root.attrs["Type"] = np.array("UnstructuredGrid".encode('ascii'), dtype=ascii_type)
    root.attrs["Version"] = np.array([2, 2])

    #-------------------------------------------------------------------------------------------------------------------
    # create general structure that will accommodate the geometry data
    # maxshape = (None, 0) indicates that the dataset will be resizable, enable chunking and that we will be
    # appending to these later
    #-------------------------------------------------------------------------------------------------------------------

    #----- Create geometry -----#
    # create empty structure
    root.create_dataset("NumberOfPoints", (0,), maxshape=(None,), dtype='uint32')

    # create structure with points
    points = root.create_dataset("Points", (num_verts,3), dtype='float32')
    points[:] = geometry

    # ----- Create topology -----#
    # create empty structure
    root.create_dataset("NumberOfCells", (0,), maxshape=(None,), dtype='uint32')

    # create empty structure
    root.create_dataset("NumberOfConnectivityIds", (0,), maxshape=(None,), dtype='uint32')

    # create structure with connectivity
    connectivity = root.create_dataset("Connectivity", num_conns, dtype='int32')
    connectivity[:] = topology.flatten()

    # create offsets (vertices per cell)
    offsets = root.create_dataset("Offsets", num_cells + 1, dtype='int32')
    offsets[:] = np.arange(0, num_conns + num_verts_per_cell, num_verts_per_cell)

    # add cell types (VTK nomenclature)
    types = root.create_dataset("Types", num_cells, dtype='uint8')
    types[:] = np.ones(num_cells) * cell_type

    # ----- Create group for point data -----#
    # create empty structure
    point_data = root.create_group('PointData')
    point_data.create_dataset('Vm', (0,), maxshape=(None,), chunks=num_verts, dtype='float32')


    # ----- Create group for steps that holds time-dependent meta-data -----#
    steps = root.create_group('Steps')
    steps.attrs['NSteps'] = 0

    # initialize values dataset to hold the values of each time step
    steps.create_dataset('Values', (0,), maxshape=(None,), dtype='uint32')

    # initialize part offsetting dataset
    # tell reader which part to go to start reading the given time step
    steps.create_dataset('PartOffsets', (0,), maxshape=(None,), dtype='uint8')

    #  create the geometry offsetting structures holding the information of where to start reading point coordinates
    steps.create_dataset('PointOffsets', (0,), maxshape=(None,), dtype='uint8')

    # equivalent topological offsetting structures
    steps.create_dataset('CellOffsets', (0,), maxshape=(None,), dtype='uint8')
    steps.create_dataset('ConnectivityIdOffsets', (0,), maxshape=(None,), dtype='uint8')

    # initialize the point data offsetting structures to determine the offsets related to our scalar data
    # defined on the points
    point_data_offsets = steps.create_group('PointDataOffsets')
    point_data_offsets.create_dataset('Vm', (0,), maxshape=(None,), dtype='uint64')


def write_vtkhdf(file_path:str, data:np.ndarray, num_geom:dict, ts_next, stride:int):
    """
    Write vtkhdf file.

    Args:
        file_path (str): Contains file path of the mesh.
        data (np.ndarray): Contains data of a chunk.
        num_geom (dict): Contains number of cells, points, connections and points per cell.
        ts_next (Any): Contains file part of a chuck to write.
        stride (int): Contains stride in case time steps should be skipped.

    Returns:
        Any: Part of the file defined by chunck to be stored.
    """
    # get number of cells, points, connections and points per cell
    num_cells = num_geom["num_cells"]
    num_verts = num_geom["num_verts"]
    num_conns = num_geom["num_conns"]
    num_ts = data.shape[0]

    # append to file
    f = h5py.File(file_path, "a")

    # get names of data sets
    root = f["VTKHDF"]
    steps = root["Steps"]
    point_data = root["PointData"]
    point_data_offsets = steps["PointDataOffsets"]

    # adds given array to corresponding HDF5 data set
    def append_dataset(dset, array):
        original_length = dset.shape[0]
        dset.resize(original_length + array.shape[0], axis=0)
        dset[original_length:] = array

    # get index to store next time step in the output file
    t_ind_write = int(steps.attrs["NSteps"])

    # get index of next time step in the current data chunk according to stride criteria (in the input file)
    t_ind_read = t_ind_write * stride - ts_next

    for i_ts, ts in enumerate(range(t_ind_read, num_ts, stride)):
        # update number of time steps
        steps.attrs["NSteps"] += 1

        # update value for time step
        append_dataset(steps["Values"], np.array([int(ts_next + ts)]))

        # set offsets to 0 (static mesh)
        append_dataset(steps["PartOffsets"], np.array([0]))
        append_dataset(steps["PointOffsets"], np.array([0]))
        append_dataset(steps["CellOffsets"], np.array([0]))
        append_dataset(steps["ConnectivityIdOffsets"], np.array([0]))

        # compute offset for point data
        t_ind_write_offset = int(num_verts * (t_ind_write + i_ts))
        # append offset for point data
        append_dataset(point_data_offsets["Vm"], np.array([t_ind_write_offset]))

        # append number of points, cells and connections
        append_dataset(root["NumberOfPoints"], np.array([num_verts]))
        append_dataset(root["NumberOfCells"], np.array([num_cells]))
        append_dataset(root["NumberOfConnectivityIds"], np.array([num_conns]))

        # append scalar data
        append_dataset(point_data["Vm"], data[ts, :])

        if (ts_next + ts) % stride == 0:
            print(f"Current time step: {ts_next + ts}")

    return ts_next + num_ts


if __name__ == '__main__':
    parsed_args = arg_parser()
    main(parsed_args)
