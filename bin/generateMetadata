#!/usr/bin/env python3

"""
@package bin.generateMetadata
Create a copy of the metadata template for experiments.

Copy the metadata template for experiments in the location provided as
a command-line argument, with the name `codemeta.json`.
If no argument is given, the template is copied in the current working directory.

Example of usage:
```
./generateMetadata /path/to/experiment
```
This creates the file `/path/to/experiment/codemeta.json`, if `/path/to/experiment`
is the path to an existing directory. 
"""

from carputils import resources
import os.path
import sys


def generate_metadata_file(location='.', metadata_filename='codemeta.json'):
    """
    Copy the metadata template in the `location` directory.
    """
    if not os.path.isdir(location):
        raise SystemExit(f'Aborting: {location} is not an existing directory.')
    metadata_location = os.path.join(location, metadata_filename)
    if os.path.exists(metadata_location):
        raise SystemExit(f'Aborting: {metadata_location} already exists.')
    metadata_s = resources.load(f'{metadata_filename}.tpl')
    f_dest = open(metadata_location, 'w')
    f_dest.write(metadata_s)
    f_dest.close()


def main(argv):
    """
    Copy the metadata template at the location provided in `argv[1]`,
    or in the current working directory if `argv` contains less than 2 elements.
    """
    metadata_filename = 'codemeta.json'
    if len(argv) <= 1:
        location = '.'
    else:
        location = argv[1]
    generate_metadata_file(location, metadata_filename)
    print('Metadata file created at {}'.format(os.path.join(os.path.abspath(location), metadata_filename)))


if __name__ == '__main__':
    main(sys.argv)