#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
author: Matthias A.F. Gsell
email: gsell.matthias@gmail.com
date: 03-01-2020
"""

from setuptools import setup, find_packages, Extension
from sysconfig import get_paths
import numpy

MAJOR_VERSION = 1
MINOR_VERSION = 8
PATCH_LEVEL = 0

VERSION = '{}.{}.{}'.format(MAJOR_VERSION, MINOR_VERSION, PATCH_LEVEL)

def main():
  
  macros = [('MAJOR_VERSION', str(MAJOR_VERSION)),
            ('MINOR_VERSION', str(MINOR_VERSION)),
            ('PATCH_LEVEL', str(PATCH_LEVEL)),
            ('VERSION', VERSION)]
  
  compile_args = ['-std=c99', 
                  '-Wno-unused-result', 
                  '-Wno-incompatible-pointer-types']

  link_args = []
  modules = []
  incl_dirs = [numpy.get_include(), get_paths()['include']]

  mshread_ext = Extension('mshread', sources=['src/mshread.c'], 
                          define_macros=macros,
                          include_dirs=incl_dirs,
                          extra_compile_args=compile_args,
                          extra_link_args=link_args)
  modules.append(mshread_ext)

  mshwrite_ext = Extension('mshwrite', sources=['src/mshwrite.c'],
                           define_macros=macros,
                           include_dirs=incl_dirs,
                           extra_compile_args=compile_args,
                           extra_link_args=link_args)
  modules.append(mshwrite_ext)

  mshutils_ext = Extension('mshutils', sources=['src/mshutils.c'],
                           define_macros=macros,
                           include_dirs=incl_dirs,
                           extra_compile_args=compile_args,
                           extra_link_args=link_args)
  modules.append(mshutils_ext)

  mshdata_ext = Extension('mshdata', sources=['src/mshdata.c'],
                           define_macros=macros,
                           include_dirs=incl_dirs,
                           extra_compile_args=compile_args,
                           extra_link_args=link_args)
  modules.append(mshdata_ext)
  
  setup(name='carputils_cext',
        description='Collection of routines to handle openCARP mesh related data',
        version=VERSION,
        author="Matthias A.F. Gsell",
        author_email="gsell.matthias@gmail.com",
        ext_package='carputils.cext',
        ext_modules=modules)

if __name__ == '__main__':
  main()
