/*
 * author: Matthias A.F. Gsell
 * email: gsell.matthias@gmail.com
 * date: 03-01-2020
 */

// Disable deprecated API versions
#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <Python.h>
#include <numpy/arrayobject.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 512

static int cmpfunc_int(const void *a, const void *b) {
   return (*(int*)a - *(int*)b);
}

PyDoc_STRVAR(doc_retag_elements,
    "retag_elements(filename:string, tags:string, output:string) -> None\n\n"
    "Function to retag an openCARP elements file and to write it\n"
    "to a new openCARP elements file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the input elements file (*.elem)\n"
    "  tags     : string\n"
    "             path to the input tags file (*.dat)\n"
    "  output   : string\n"
    "             path to the output elements file (*.elem)\n"
);

// retag a given element file
static PyObject *retag_elements(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filenames from the arguments
  char *elm_fname = NULL;
  char *tag_fname = NULL;
  char *out_fname = NULL;
  if (!PyArg_ParseTuple(args, "sss", &elm_fname, &tag_fname, &out_fname))
    return NULL;
  // open element file
  FILE *elm_fp = fopen(elm_fname, "r");
  if (!elm_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", elm_fname);
    return NULL;
  }
  // open tag file
  FILE *tag_fp = fopen(tag_fname, "r");
  if (!tag_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", tag_fname);
    return NULL;
  }
  // open new element file
  FILE *out_fp = fopen(out_fname, "w");
  if (!out_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", out_fname);
    return NULL;
  }

  // element type and data
  char type[4];
  int new_tag, idx[8], tag;
  // read number of elements
  int num;
  fscanf(elm_fp, "%d", &num);
  // write number of elements
  fprintf(out_fp, "%d\n", num);

  // read elements and tags and write to new file
  for (int i=0; i<num; i++) {
    // read new tag from file
    fscanf(tag_fp, "%d", &new_tag);
    // get element type
    fscanf(elm_fp, "%3s", type);
    // read element indices and tags
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &tag);
      // write element and new tag
      fprintf(out_fp, "Tt %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], new_tag);
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &tag);
      // write element and new tag
      fprintf(out_fp, "Py %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], new_tag);
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      // write element and new tag
      fprintf(out_fp, "Oc %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], new_tag);
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      // write element and new tag
      fprintf(out_fp, "Pr %d %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], idx[5], new_tag);
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &idx[6], &idx[7], &tag);
      // write element and new tag
      fprintf(out_fp, "Hx %d %d %d %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], idx[5], idx[6], idx[7], new_tag);
    }
    else if (strcmp(type, "Tr") == 0) { // triangle, 'Tr'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d", &idx[0], &idx[1], &idx[2], &tag);
      // write element and new tag
      fprintf(out_fp, "Tr %d %d %d %d\n", idx[0], idx[1], idx[2], new_tag);
    }
    else if (strcmp(type, "Qd") == 0) { // quad, 'Qd'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &tag);
      // write element and new tag
      fprintf(out_fp, "Qd %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], new_tag);
    }
    else { // unknwon element type
      // close files
      fclose(out_fp);
      fclose(tag_fp);
      fclose(elm_fp);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }
  }

  // close files
  fclose(out_fp);
  fclose(tag_fp);
  fclose(elm_fp);

  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_insert_element_tags,
    "insert_element_tags(filename:string, tags:string, output:string) -> None\n\n"
    "Function to insert element tags to an openCARP elements file\n"
    "and to write it to a new openCARP elements file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the input elements file (*.elem)\n"
    "  tags     : string\n"
    "             path to the input tags file (*.dat)\n"
    "  output   : string\n"
    "             path to the output elements file (*.elem)\n"
);

// insert tags to a given element file
static PyObject *insert_element_tags(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filenames from the arguments
  char *elm_fname = NULL;
  char *tag_fname = NULL;
  char *out_fname = NULL;
  if (!PyArg_ParseTuple(args, "sss", &elm_fname, &tag_fname, &out_fname))
    return NULL;
  // open element file
  FILE *elm_fp = fopen(elm_fname, "r");
  if (!elm_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", elm_fname);
    return NULL;
  }
  // open tag file
  FILE *tag_fp = fopen(tag_fname, "r");
  if (!tag_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", tag_fname);
    return NULL;
  }
  // open new element file
  FILE *out_fp = fopen(out_fname, "w");
  if (!out_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", out_fname);
    return NULL;
  }

  // element type and data
  char type[4];
  int new_tag, idx[8], tag;
  // read number of elements
  int num;
  fscanf(elm_fp, "%d", &num);
  // write number of elements
  fprintf(out_fp, "%d\n", num);

  // read elements and tags and write to new file
  for (int i=0; i<num; i++) {
    // read new tag from file
    fscanf(tag_fp, "%d", &new_tag);
    // get element type
    fscanf(elm_fp, "%3s", type);
    // read element indices and tags
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &tag);
      if (new_tag == 0) new_tag = tag;
      // write element and new tag
      fprintf(out_fp, "Tt %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], new_tag);
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &tag);
      if (new_tag == 0) new_tag = tag;
      // write element and new tag
      fprintf(out_fp, "Py %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], new_tag);
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      if (new_tag == 0) new_tag = tag;
      // write element and new tag
      fprintf(out_fp, "Oc %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], new_tag);
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      if (new_tag == 0) new_tag = tag;
      // write element and new tag
      fprintf(out_fp, "Pr %d %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], idx[5], new_tag);
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &idx[6], &idx[7], &tag);
      if (new_tag == 0) new_tag = tag;
      // write element and new tag
      fprintf(out_fp, "Hx %d %d %d %d %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], idx[4], idx[5], idx[6], idx[7], new_tag);
    }
    else if (strcmp(type, "Tr") == 0) { // triangle, 'Tr'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d", &idx[0], &idx[1], &idx[2], &tag);
      if (new_tag == 0) new_tag = tag;
      // write element and new tag
      fprintf(out_fp, "Tr %d %d %d %d\n", idx[0], idx[1], idx[2], new_tag);
    }
    else if (strcmp(type, "Qd") == 0) { // quad, 'Qd'-element
      // read indices and tag
      fscanf(elm_fp, "%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &tag);
      if (new_tag == 0) new_tag = tag;
      // write element and new tag
      fprintf(out_fp, "Qd %d %d %d %d %d\n", idx[0], idx[1], idx[2], idx[3], new_tag);
    }
    else { // unknwon element type
      // close files
      fclose(out_fp);
      fclose(tag_fp);
      fclose(elm_fp);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }
  }

  // close files
  fclose(out_fp);
  fclose(tag_fp);
  fclose(elm_fp);

  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_extract_element_tags,
    "extract_element_tags(filename:string, tags:string) -> None\n\n"
    "Function to extract the tag data from an openCARP elements file and\n"
    "to write the tags to an openCARP data file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the input elements file (*.elem)\n"
    "  tags     : string\n"
    "             path to the output tags file (*.dat)\n"
);

// extract tags from a given element file
static PyObject *extract_element_tags(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filenames from the arguments
  char *elm_fname = NULL;
  char *out_fname = NULL;
  if (!PyArg_ParseTuple(args, "ss", &elm_fname, &out_fname))
    return NULL;
  // open element file
  FILE *elm_fp = fopen(elm_fname, "r");
  if (!elm_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", elm_fname);
    return NULL;
  }
  // open new element file
  FILE *out_fp = fopen(out_fname, "w");
  if (!out_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", out_fname);
    return NULL;
  }

  // element type and data
  char type[4];
  int tag;
  // read number of elements
  int num;
  fscanf(elm_fp, "%d", &num);

  // read elements tags and write to new file
  for (int i=0; i<num; i++) {
    // get element type
    fscanf(elm_fp, "%3s", type);
    // read element tags
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Tr") == 0) { // triangle, 'Tr'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Qd") == 0) { // quad, 'Qd'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%d", &tag);
    }
    else { // unknwon element type
      // close files
      fclose(elm_fp);
      fclose(out_fp);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }
    // write tag
    fprintf(out_fp, "%d\n", tag);
  }

  // close files
  fclose(out_fp);
  fclose(elm_fp);

  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_get_element_centers,
    "get_element_centers(filename_pts:string, filename_elem:string) -> NumpyArray[float,?x3]\n\n"
    "Function to compute the element centers from an openCARP mesh\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename_pts  : string\n"
    "                  path to the input points file (*.pts)\n"
    "  filename_elem : string\n"
    "                  path to the input elements file (*.elem)\n"
    "Returns:\n"
    "--------\n"
    "  numpy float-array of size (N x 3) holding the centers of each element"
);

// determine element centers from a given points and element file
static PyObject *get_element_centers(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filenames from the arguments
  char *pts_fname = NULL;
  char *elm_fname = NULL;
  if (!PyArg_ParseTuple(args, "ss", &pts_fname, &elm_fname))
    return NULL;
  // open point file
  FILE *pnt_fp = fopen(pts_fname, "r");
  if (!pnt_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", pts_fname);
    return NULL;
  }
  // open element file
  FILE *elm_fp = fopen(elm_fname, "r");
  if (!elm_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", elm_fname);
    return NULL;
  }

  // read number of points
  int num_pnt;
  fscanf(pnt_fp, "%d", &num_pnt);
  // allocate memory for points
  double *pnt_data = (double*)malloc(3*num_pnt*sizeof(double));
  double *pnt = NULL;

  // read popint data
  for (int i=0; i<num_pnt; i++) {
    pnt = &pnt_data[3*i];
    fscanf(pnt_fp, "%lf%lf%lf", &pnt[0], &pnt[1], &pnt[2]);
  }

  // close file
  fclose(pnt_fp);

  // read number of elements
  int num_elm;
  fscanf(elm_fp, "%d", &num_elm);
  // allocate memory  for cell centers
  double *ctr_data = (double*)malloc(3*num_elm*sizeof(double));
  double *ctr = NULL;
  // element type and data
  char type[4];
  int idx[8];

  // read elements and tags and write to new file
  for (int i=0; i<num_elm; i++) {
    ctr = &ctr_data[3*i];
    ctr[0] = ctr[1] = ctr[2] = 0.0;

    // get element type
    fscanf(elm_fp, "%3s", type);
    // read element indices
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%*d", &idx[0], &idx[1], &idx[2], &idx[3]);
      // determine element center
      ctr[0] = (pnt_data[3*idx[0]+0] + pnt_data[3*idx[1]+0] +
                pnt_data[3*idx[2]+0] + pnt_data[3*idx[3]+0])/4.0;
      ctr[1] = (pnt_data[3*idx[0]+1] + pnt_data[3*idx[1]+1] +
                pnt_data[3*idx[2]+1] + pnt_data[3*idx[3]+1])/4.0;
      ctr[2] = (pnt_data[3*idx[0]+2] + pnt_data[3*idx[1]+2] +
                pnt_data[3*idx[2]+2] + pnt_data[3*idx[3]+2])/4.0;
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%*d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4]);
      // determine element center
      ctr[0] = (pnt_data[3*idx[0]+0] + pnt_data[3*idx[1]+0] + pnt_data[3*idx[2]+0] +
                pnt_data[3*idx[3]+0] + pnt_data[3*idx[4]+0])/5.0;
      ctr[1] = (pnt_data[3*idx[0]+1] + pnt_data[3*idx[1]+1] + pnt_data[3*idx[2]+1] +
                pnt_data[3*idx[3]+1] + pnt_data[3*idx[4]+1])/5.0;
      ctr[2] = (pnt_data[3*idx[0]+2] + pnt_data[3*idx[1]+2] + pnt_data[3*idx[2]+2] +
                pnt_data[3*idx[3]+2] + pnt_data[3*idx[4]+2])/5.0;
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%*d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5]);
      // determine element center
      ctr[0] = (pnt_data[3*idx[0]+0] + pnt_data[3*idx[1]+0] + pnt_data[3*idx[2]+0] +
                pnt_data[3*idx[3]+0] + pnt_data[3*idx[4]+0] + pnt_data[3*idx[5]+0])/6.0;
      ctr[1] = (pnt_data[3*idx[0]+1] + pnt_data[3*idx[1]+1] + pnt_data[3*idx[2]+1] +
                pnt_data[3*idx[3]+1] + pnt_data[3*idx[4]+1] + pnt_data[3*idx[5]+1])/6.0;
      ctr[2] = (pnt_data[3*idx[0]+2] + pnt_data[3*idx[1]+2] + pnt_data[3*idx[2]+2] +
                pnt_data[3*idx[3]+2] + pnt_data[3*idx[4]+2] + pnt_data[3*idx[5]+2])/6.0;
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%*d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5]);
      // determine element center
      ctr[0] = (pnt_data[3*idx[0]+0] + pnt_data[3*idx[1]+0] + pnt_data[3*idx[2]+0] +
                pnt_data[3*idx[3]+0] + pnt_data[3*idx[4]+0] + pnt_data[3*idx[5]+0])/6.0;
      ctr[1] = (pnt_data[3*idx[0]+1] + pnt_data[3*idx[1]+1] + pnt_data[3*idx[2]+1] +
                pnt_data[3*idx[3]+1] + pnt_data[3*idx[4]+1] + pnt_data[3*idx[5]+1])/6.0;
      ctr[2] = (pnt_data[3*idx[0]+2] + pnt_data[3*idx[1]+2] + pnt_data[3*idx[2]+2] +
                pnt_data[3*idx[3]+2] + pnt_data[3*idx[4]+2] + pnt_data[3*idx[5]+2])/6.0;
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d%d%*d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &idx[6], &idx[7]);
      // determine element center
      ctr[0] = (pnt_data[3*idx[0]+0] + pnt_data[3*idx[1]+0] + pnt_data[3*idx[2]+0] +
                pnt_data[3*idx[3]+0] + pnt_data[3*idx[4]+0] + pnt_data[3*idx[5]+0] +
                pnt_data[3*idx[6]+0] + pnt_data[3*idx[7]+0])/8.0;
      ctr[1] = (pnt_data[3*idx[0]+1] + pnt_data[3*idx[1]+1] + pnt_data[3*idx[2]+1] +
                pnt_data[3*idx[3]+1] + pnt_data[3*idx[4]+1] + pnt_data[3*idx[5]+1] +
                pnt_data[3*idx[6]+1] + pnt_data[3*idx[7]+1])/8.0;
      ctr[2] = (pnt_data[3*idx[0]+2] + pnt_data[3*idx[1]+2] + pnt_data[3*idx[2]+2] +
                pnt_data[3*idx[3]+2] + pnt_data[3*idx[4]+2] + pnt_data[3*idx[5]+2] +
                pnt_data[3*idx[6]+2] + pnt_data[3*idx[7]+2])/8.0;
    }
    else if (strcmp(type, "Tr") == 0) { // trinagle, 'Tr'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%*d", &idx[0], &idx[1], &idx[2]);
      // determine element center
      ctr[0] = (pnt_data[3*idx[0]+0] + pnt_data[3*idx[1]+0] +
                pnt_data[3*idx[2]+0])/3.0;
      ctr[1] = (pnt_data[3*idx[0]+1] + pnt_data[3*idx[1]+1] +
                pnt_data[3*idx[2]+1])/3.0;
      ctr[2] = (pnt_data[3*idx[0]+2] + pnt_data[3*idx[1]+2] +
                pnt_data[3*idx[2]+2])/3.0;
    }
    else if (strcmp(type, "Qd") == 0) { // quad, 'Qd'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%*d", &idx[0], &idx[1], &idx[2], &idx[3]);
      // determine element center
      ctr[0] = (pnt_data[3*idx[0]+0] + pnt_data[3*idx[1]+0] +
                pnt_data[3*idx[2]+0] + pnt_data[3*idx[3]+0])/4.0;
      ctr[1] = (pnt_data[3*idx[0]+1] + pnt_data[3*idx[1]+1] +
                pnt_data[3*idx[2]+1] + pnt_data[3*idx[3]+1])/4.0;
      ctr[2] = (pnt_data[3*idx[0]+2] + pnt_data[3*idx[1]+2] +
                pnt_data[3*idx[2]+2] + pnt_data[3*idx[3]+2])/4.0;
    }
    else { // unknwon element type
      // close file
      fclose(elm_fp);

      // free memory
      free(pnt_data);
      free(ctr_data);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }
  }
  // close file
  fclose(elm_fp);

  // free memory
  free(pnt_data);

  // convert to numpy array
  npy_intp array_dim[2] = {num_elm, 3};
  PyArrayObject *ctr_array = PyArray_SimpleNewFromData(2, array_dim, NPY_DOUBLE, ctr_data);
  // return numpy array
  return PyArray_Return(ctr_array);
}

PyDoc_STRVAR(doc_get_mesh_center,
    "get_mesh_center(filename:string) -> Tuple[float,float,float]\n\n"
    "Function to compute the center of an openCARP mesh\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename  : string\n"
    "              path to the input points file (*.pts)\n"
    "Returns:\n"
    "--------\n"
    "  tuple holding x, y and z component of the center point"
);

// get_mesh_center
static PyObject *get_mesh_center(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  if (!PyArg_ParseTuple(args, "s", &fname))
    return NULL;
  // open point file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // read number of points
  int num;
  fscanf(fp, "%d", &num);
  // mesh and center point
  double pnt[3], ctr_pnt[3];
  ctr_pnt[0] = ctr_pnt[1] = ctr_pnt[2] = 0.0;
  // read data
  for (int i=0; i<num; i++) {
    fscanf(fp, "%lf%lf%lf", &pnt[0], &pnt[1], &pnt[2]);
    ctr_pnt[0] += (pnt[0]-ctr_pnt[0])/(1.0+i);
    ctr_pnt[1] += (pnt[1]-ctr_pnt[1])/(1.0+i);
    ctr_pnt[2] += (pnt[2]-ctr_pnt[2])/(1.0+i);
  }

  // close file
  fclose(fp);

  // return center coordinates
  return Py_BuildValue("ddd", ctr_pnt[0], ctr_pnt[1], ctr_pnt[2]);
}

PyDoc_STRVAR(doc_zero_fibers,
    "zero_fibers(filename_elem:string, filename_lon:string, tags:NumpyArray[int32,?], output:string) -> int\n\n"
    "Function to comput the elements center from an openCARP mesh\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename_elem : string\n"
    "                  path to the input elements file (*.elem)\n"
    "  filename_lon  : string\n"
    "                  path to the input fibers file (*.lon)\n"
    "  tags          : numpy int32-array\n"
    "                  tags of the elements whose fibers should be zeroed\n"
    "                  (the array will be sorted after calling this method)\n"
    "  output        : string\n"
    "                  path to the output fibers file (*.lon)\n"
    "Returns:\n"
    "--------\n"
    "  number of elements whose fibers were zeroed"
);

// determine element centers from a given points and element file
static PyObject *zero_fibers(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filenames from the arguments
  char *elm_fname = NULL;
  char *lon_fname = NULL;
  PyArrayObject *tag_array = NULL;
  char *out_fname = NULL;
  if (!PyArg_ParseTuple(args, "ssO!s", &elm_fname, &lon_fname, &PyArray_Type, &tag_array, &out_fname))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(tag_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(tag_array);
  if (dtype != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(tag_array);
  if (ndim != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
    return NULL;
  }
  // open fiber file
  FILE *lon_fp = fopen(lon_fname, "r");
  if (!lon_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", lon_fname);
    return NULL;
  }

  char line[BUFFER_SIZE];
  // read first line in fiber file
  fgets(line, BUFFER_SIZE, lon_fp);
  // get fiber dimension
  int dim;
  sscanf(line, "%d", &dim);
  if ((dim != 1) && (dim != 2)) {
    // close file
    fclose(lon_fp);

    // raise exception if dimension was neither 1 nor 2
    PyErr_Format(PyExc_ValueError, "Error, dimension %d not supported!", dim);
    return NULL;
  }

  const char *zero_fiber = ((dim==1) ? "0 0 0\n" : "0 0 0 0 0 0\n");
  // open element file
  FILE *elm_fp = fopen(elm_fname, "r");
  if (!elm_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", elm_fname);
    return NULL;
  }
  // open output file
  FILE *out_fp = fopen(out_fname, "w");
  if (!out_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", out_fname);
    return NULL;
  }
  fprintf(out_fp, "%d\n", dim);

  // sort tag data for faster searching
  const npy_intp num_tags = PyArray_DIM(tag_array, 0);
  int *tag_data = (int*)PyArray_DATA(tag_array);
  qsort(tag_data, num_tags, sizeof(int), cmpfunc_int);

  // read number of elements
  int num, count=0;
  fscanf(elm_fp, "%d", &num);
  // element type and data
  char type[4];
  int tag;

  // read elements and tags and write to new file
  for (int i=0; i<num; i++) {
    // get element type
    fscanf(elm_fp, "%3s", type);
    // read element tags
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%*d%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Tr") == 0) { // triangle, 'Tr'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%d", &tag);
    }
    else if (strcmp(type, "Qd") == 0) { // quad, 'Qd'-element
      // read tag
      fscanf(elm_fp, "%*d%*d%*d%*d%d", &tag);
    }
    else { // unknwon element type
      // close files
      fclose(elm_fp);
      fclose(lon_fp);
      fclose(out_fp);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }

    // read original fibers
    fgets(line, BUFFER_SIZE, lon_fp);
    if (bsearch(&tag, tag_data, num_tags, sizeof(int), cmpfunc_int)) { // found
      // write zeores
      fputs(zero_fiber, out_fp);
      count++;
    } else { // not found
      // write original fibers
      fputs(line, out_fp);
    }
  }
  // close file
  fclose(elm_fp);
  fclose(lon_fp);
  fclose(out_fp);

  // return number of changes
  return Py_BuildValue("i", count);
}

static int is_double_vector(PyArrayObject* array) {
  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(array);
  if (!iscont)
    return -1;
  // get array data type
  const int dtype = PyArray_TYPE(array);
  if (dtype != NPY_DOUBLE)
    return -2;
  // get array dimension
  const int ndim = PyArray_NDIM(array);
  if (ndim != 1)
    return -3;
  // get array shape
  const npy_intp *shape = PyArray_DIMS(array);
  if (shape[0] != 3)
    return -4;
  return 0;
}

PyDoc_STRVAR(doc_transform_points,
    "transform_points(filename:string, output:string, *, center:bool, rotation:NumpyArray[float,3], \
     scale:NumpyArray[float,3], translate:NumpyArray[float,3]) -> None\n\n"
    "Function to transform points from an openCARP points file by the mapping\n"
    "     p_new = R . S . (p_old - T0) + T\n"
    "where T0 is either the center of the point cloud or zero,\n"
    "R is the rotation matrix R = Rz.Ry.Rx,\n"
    "S is the scaling diagonal-matrix and T is the translation\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename  : string\n"
    "              path to the input points file (*.pts)\n"
    "  output    : str\n"
    "              path to the output points file (*.pts)\n"
    "  center    : bool, optional, default=True\n"
    "              if true, mesh is centered before transformation\n"
    "  rotate    : numpy float-array of size 3, optional, default=(0.0, 0.0, 0.0)\n"
    "              angles of the rotation matrices Rx, Ry and Rz\n"
    "  scale     : numpy float-array of size 3, optional, default=(1.0, 1.0, 1.0)\n"
    "              scaling factors\n"
    "  translate : numpy float-array of size 3, optional, default=(0.0, 0.0, 0.0)\n"
    "              translation vector"
);

// transform point cloud
static PyObject *transform_points(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *pts_fname = NULL;
  char *out_fname = NULL;
  int center = 1;
  PyArrayObject *rot_array = NULL;
  PyArrayObject *scl_array = NULL;
  PyArrayObject *tsl_array = NULL;
  static char *kwlist[] = {"", "", "center", "rotate", "scale", "translate", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "ss|pO!O!O!", kwlist, &pts_fname, &out_fname, &center,
                                                                      &PyArray_Type, &rot_array,
                                                                      &PyArray_Type, &scl_array,
                                                                      &PyArray_Type, &tsl_array))
    return NULL;

  // get rotation data
  const double default_rot_data[3] = {0.0, 0.0, 0.0};
  if ((rot_array) && (is_double_vector(rot_array) != 0)) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional double vector of size 3 expected!");
    return NULL;
  }
  const double *rot_data = (rot_array) ? (const double*)PyArray_DATA(rot_array) : default_rot_data;

  // get scaling data
  const double default_scl_data[3] = {1.0, 1.0, 1.0};
  if ((scl_array) && (is_double_vector(scl_array) != 0)) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional double vector of size 3 expected!");
    return NULL;
  }
  const double *scl_data = (scl_array) ? (const double*)PyArray_DATA(scl_array) : default_scl_data;

  // get translation data
  const double default_tsl_data[3] = {0.0, 0.0, 0.0};
  if ((tsl_array) && (is_double_vector(tsl_array) != 0)) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional double vector of size 3 expected!");
    return NULL;
  }
  const double *tsl_data = (tsl_array) ? (const double*)PyArray_DATA(tsl_array) : default_tsl_data;

  // open points file
  FILE *pts_fp = fopen(pts_fname, "r");
  if (!pts_fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", pts_fname);
    return NULL;
  }

  // open output file
  FILE *out_fp = fopen(out_fname, "w");
  if (!out_fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", out_fname);
    return NULL;
  }

  // read number of points
  int num;
  fscanf(pts_fp, "%d", &num);
  // allocate memory
  double *pnt_data = (double*)malloc(3*num*sizeof(double));
  double *pnt = NULL;
  // center point
  double ctr_pnt[3];
  ctr_pnt[0] = ctr_pnt[1] = ctr_pnt[2] = 0.0;
  // read data
  for (int i=0; i<num; i++) {
    pnt = &pnt_data[3*i];
    fscanf(pts_fp, "%lf%lf%lf", &pnt[0], &pnt[1], &pnt[2]);
    ctr_pnt[0] += (pnt[0]-ctr_pnt[0])/(1.0+i);
    ctr_pnt[1] += (pnt[1]-ctr_pnt[1])/(1.0+i);
    ctr_pnt[2] += (pnt[2]-ctr_pnt[2])/(1.0+i);
  }

  // close points file
  fclose(pts_fp);

  // setup rotation matrix
  double rot_mat[9];
  rot_mat[0] = cos(rot_data[1])*cos(rot_data[2]);
  rot_mat[1] = sin(rot_data[0])*sin(rot_data[1])*cos(rot_data[2])-cos(rot_data[0])*sin(rot_data[2]);
  rot_mat[2] = sin(rot_data[0])*sin(rot_data[2])+cos(rot_data[0])*sin(rot_data[1])*cos(rot_data[2]);
	rot_mat[3] = cos(rot_data[1])*sin(rot_data[2]);
  rot_mat[4] = sin(rot_data[0])*sin(rot_data[1])*sin(rot_data[2])+cos(rot_data[0])*cos(rot_data[2]);
  rot_mat[5] = cos(rot_data[0])*sin(rot_data[1])*sin(rot_data[2])-sin(rot_data[0])*cos(rot_data[2]);
	rot_mat[6] = -sin(rot_data[1]);
  rot_mat[7] = sin(rot_data[0])*cos(rot_data[1]);
  rot_mat[8] = cos(rot_data[0])*cos(rot_data[1]);

  // transform points and write output file
  fprintf(out_fp, "%d\n", num);
  // t0 translation point
  double t0_pnt[3];
  t0_pnt[0] = t0_pnt[1] = t0_pnt[2] = 0.0;
  if (center) {
    t0_pnt[0] = ctr_pnt[0];
    t0_pnt[1] = ctr_pnt[1];
    t0_pnt[2] = ctr_pnt[2];
  }
  double tmp_pnt[3];
  for (int i=0; i<num; i++) {
    pnt = &pnt_data[3*i];
    tmp_pnt[0] = rot_mat[0]*scl_data[0]*(pnt[0]-t0_pnt[0]) + rot_mat[1]*scl_data[1]*(pnt[1]-t0_pnt[1]) +
                 rot_mat[2]*scl_data[2]*(pnt[2]-t0_pnt[2]) + tsl_data[0];
    tmp_pnt[1] = rot_mat[3]*scl_data[0]*(pnt[0]-t0_pnt[0]) + rot_mat[4]*scl_data[1]*(pnt[1]-t0_pnt[1]) +
                 rot_mat[5]*scl_data[2]*(pnt[2]-t0_pnt[2]) + tsl_data[1];
    tmp_pnt[2] = rot_mat[6]*scl_data[0]*(pnt[0]-t0_pnt[0]) + rot_mat[7]*scl_data[1]*(pnt[1]-t0_pnt[1]) +
                 rot_mat[8]*scl_data[2]*(pnt[2]-t0_pnt[2]) + tsl_data[2];
    fprintf(out_fp, "%lf %lf %lf\n", tmp_pnt[0], tmp_pnt[1], tmp_pnt[2]);
  }
  // free point data
  free(pnt_data);

  // close output file
  fclose(out_fp);

  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_get_bounding_box,
    "get_bounding_box(filename:string) -> NumpyArray[float,2x3]\n\n"
    "Get the bounding box of an openCARP points file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename  : string\n"
    "              path to the input points file (*.pts)\n"
    "Returns:\n"
    "--------\n"
    "  numpy float-array of size (2 x 3) holding the two\n"
    "  bounding-box the points\n"
);

// get bounding-box of a point cloud
static PyObject *get_bounding_box(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *pts_fname = NULL;
  if (!PyArg_ParseTuple(args, "s", &pts_fname))
    return NULL;

  // open points file
  FILE *pts_fp = fopen(pts_fname, "r");
  if (!pts_fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", pts_fname);
    return NULL;
  }

  // read number of points
  int num_pts;
  fscanf(pts_fp, "%d", &num_pts);
  // allocate bounding box memory
  double *box_data = (double*)malloc(6*sizeof(double));
  // current point
  double pnt[3];
  // read first point
  fscanf(pts_fp, "%lf%lf%lf", &pnt[0], &pnt[1], &pnt[2]);
  // initialize bounding box with first point
  box_data[0] = box_data[3] = pnt[0];
  box_data[1] = box_data[4] = pnt[1];
  box_data[2] = box_data[5] = pnt[2];
  // read remaining points and update bounding box
  for (int i=1; i<num_pts; i++) {
    fscanf(pts_fp, "%lf%lf%lf", &pnt[0], &pnt[1], &pnt[2]);
    // update bounding box
    if (pnt[0] < box_data[0]) box_data[0] = pnt[0];
    if (pnt[0] > box_data[3]) box_data[3] = pnt[0];
    if (pnt[1] < box_data[1]) box_data[1] = pnt[1];
    if (pnt[1] > box_data[4]) box_data[4] = pnt[1];
    if (pnt[2] < box_data[2]) box_data[2] = pnt[2];
    if (pnt[2] > box_data[5]) box_data[5] = pnt[2];
  }

  // close points file
  fclose(pts_fp);

  // convert to numpy array
  npy_intp array_dim[2] = {2, 3};
  PyArrayObject *box_array = PyArray_SimpleNewFromData(2, array_dim, NPY_DOUBLE, box_data);
  // return numpy array
  return PyArray_Return(box_array);
}

PyDoc_STRVAR(doc_mesh_type,
    "mesh_type(filename:string) -> int\n\n"
    "Get the type of an openCARP elements file\n\n"
    "to a new openCARP elements file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the input elements file (*.elem)\n"
    "Returns:\n"
    "--------\n"
    "  int representing the type of the mesh (1..surface, 2..volumetric, 3..hybrid)"
);

// retag a given element file
static PyObject *mesh_type(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filenames from the arguments
  char *elm_fname = NULL;
  if (!PyArg_ParseTuple(args, "s", &elm_fname))
    return NULL;
  // open element file
  FILE *fp = fopen(elm_fname, "r");
  if (!fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", elm_fname);
    return NULL;
  }

  // element type and data
  char type[4];
  int mesh_type = 0;

  // read number of elements
  int num;
  fscanf(fp, "%d", &num);

  // allocate memory
  char *line_buffer = (char*)malloc(BUFFER_SIZE*sizeof(char));
  size_t line_length = 0;
  // get remaining part of the header line
  getline(&line_buffer, &line_length, fp);

  // read element types
  for (int i=0; i<num; i++) {
    getline(&line_buffer, &line_length, fp);
    // get element type
    sscanf(line_buffer, "%3s", type);
    // read element indices and tags
    if (strcmp(type, "Tt") == 0) // tetrahedron, 'Tt'-element
      mesh_type |= 2;
    else if (strcmp(type, "Py") == 0) // pyramid, 'Py'-element
      mesh_type |= 2;
    else if (strcmp(type, "Oc") == 0) // octahedron, 'Oc'-element
      mesh_type |= 2;
    else if (strcmp(type, "Pr") == 0) // prism, 'Pr'-element
      mesh_type |= 2;
    else if (strcmp(type, "Hx") == 0) // hexahedron, 'Hx'-element
      mesh_type |= 2;
    else if (strcmp(type, "Tr") == 0) // triangle, 'Tr'-element
      mesh_type |= 1;
    else if (strcmp(type, "Qd") == 0) // quad, 'Qd'-element
      mesh_type |= 1;
    else { // unknwon element type
      // free memory
      free(line_buffer);
      // close files  
      fclose(fp);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }
  }

  // free memory
  free(line_buffer);
  // close files
  fclose(fp);

  // return type
  return Py_BuildValue("i", mesh_type);
}

// define methods
static PyMethodDef MshUtilsMethods[] = {
    {"retag_elements", (PyCFunction)retag_elements, METH_VARARGS|METH_KEYWORDS, doc_retag_elements},
    {"insert_element_tags", (PyCFunction)insert_element_tags, METH_VARARGS|METH_KEYWORDS, doc_insert_element_tags},
    {"extract_element_tags", (PyCFunction)extract_element_tags, METH_VARARGS|METH_KEYWORDS, doc_extract_element_tags},
    {"get_element_centers", (PyCFunction)get_element_centers, METH_VARARGS|METH_KEYWORDS, doc_get_element_centers},
    {"get_mesh_center", (PyCFunction)get_mesh_center, METH_VARARGS|METH_KEYWORDS, doc_get_mesh_center},
    {"zero_fibers", (PyCFunction)zero_fibers, METH_VARARGS|METH_KEYWORDS, doc_zero_fibers},
    {"transform_points", (PyCFunction)transform_points, METH_VARARGS|METH_KEYWORDS, doc_transform_points},
    {"get_bounding_box", (PyCFunction)get_bounding_box, METH_VARARGS|METH_KEYWORDS, doc_get_bounding_box},
    {"mesh_type", (PyCFunction)mesh_type, METH_VARARGS|METH_KEYWORDS, doc_mesh_type},
    {NULL, NULL, 0, NULL}
};

PyDoc_STRVAR(doc_mod_mshutils, "Module providing several functions to manipulate openCARP meshes.\n");

#if PY_MAJOR_VERSION >= 3

static struct PyModuleDef MshUtilsModule = {
    PyModuleDef_HEAD_INIT,
    "mshutils",       /* name of module */
    doc_mod_mshutils, /* module documentation, may be NULL */
    -1,               /* size of per-interpreter state of the module,
                         or -1 if the module keeps state in global variables. */
    MshUtilsMethods
};

PyMODINIT_FUNC PyInit_mshutils(void)
{
    // initialize module
    PyObject *mod = PyModule_Create(&MshUtilsModule);
    // import numpy module
    import_array();

    return mod;
}

#else

PyMODINIT_FUNC initmshutils(void)
{
    // initialize module
    PyObject *mod = Py_InitModule3("mshutils", MshUtilsMethods, doc_mod_mshutils);
    // import numpy module
    import_array();

    (void) mod;
}

#endif

