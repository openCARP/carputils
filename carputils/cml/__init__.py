#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
from carputils.cml.cmlconverter import *
from carputils.cml.cmlparser import *
from carputils.cml.cmltree import *

## Default YAML parser
YAMLParser = CMLParserYAML(emptyValue='')


def load_as_dict(fp, parser=YAMLParser):
    """
    Parse data from a text file

    Args:
        fp: File pointer to the text file
            to be parsed

    Kwargs:
        parser: Parser to use, must be an
                instance of cmlparser.CMLParser

    Returns:
        A dictionary data tree
    """
    root = parser.parsef(fp)
    return root.serialize()[1]


def loads_as_dict(string, parser=YAMLParser):
    """
    Parse data from a string

    Args:
        string: String to be parsed

    Kwargs:
        parser: Parser to use, must be an
                instance of cmlparser.CMLParser

    Returns:
        A dictionary data tree
    """
    root = parser.parses(string)
    return root.serialize()[1]


def load_as_dict_flat(fp, parser=YAMLParser):
    """
    Parse data from a text file

    Args:
        fp: File pointer to the text file
            to be parsed

    Kwargs:
        parser: Parser to use, must be an
                instance of cmlparser.CMLParser

    Returns:
        A flattened dictionary data tree
    """
    root = parser.parsef(fp)
    return CMLDictOperations.flatten(root.serialize()[1])


def loads_as_dict_flat(string, parser=YAMLParser):
    """
    Parse data from a string

    Args:
        string: String to be parsed

    Kwargs:
        parser: Parser to use, must be an
                instance of cmlparser.CMLParser

    Returns:
        A flattened dictionary data tree
    """
    root = parser.parses(string)
    return CMLDictOperations.flatten(root.serialize()[1])


def load_as_object(fp, parser=YAMLParser):
    """
    Parse data from a text file

    Args:
        fp: File pointer to the text file
            to be parsed

    Kwargs:
        parser: Parser to use, must be an
                instance of cmlparser.CMLParser

    Returns:
        A cmltree.CMLNamespace object representing
        the root of the data tree
    """
    root = parser.parsef(fp)
    return root.objectify()[1]


def loads_as_object(string, parser=YAMLParser):
    """
    Parse data from a string

    Args:
        string: String to be parsed

    Kwargs:
        parser: Parser to use, must be an
                instance of cmlparser.CMLParser

    Returns:
        A cmltree.CMLNamespace object representing
        the root of the data tree
    """
    root = parser.parses(string)
    return root.objectify()[1]

## Default method to parse data from a file
load = load_as_dict

## Default method to parse data from a string
loads = loads_as_dict

"""
def load(fp, parser=YAMLParser):
    return load_as_dict(fp, parser=parser)

def loads(string, parser=YAMLParser):
    return loads_as_dict(string, parser=parser)
"""