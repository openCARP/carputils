#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Functions to assist in setting up fibres in generated meshes.
"""

import numpy as np

def linear_fibre_rule(endo, epi, unit='degrees'):
    """
    Generate a linear fibre rule in the format expected by
    :class:`carputils.mesh.Ring` and :class:`carputils.mesh.Ellipsoid`.

    For example, for a +60/-60 fibre rule:

    >>> rule = linear_fibre_rule(60, -60)
    >>> geom = Ring(5, fibre_rule=rule)

    For more details on mesh generation and definition of fibres, see
    :ref:`generating_fibres`.

    Args:
        endo : float
            Fibre helix angle on the endocardium
        epi : float
            Fibre helix angle on the epicardium
        unit : str, optional
            One of ['degrees', 'radians'], the type of units endo and epi are
            specified in, defaults to degrees
    """

    # Get right scale factor
    scale = {'degrees': np.pi/180,
             'radians': 1.0}

    # Scale values
    endo *= scale[unit]
    epi  *= scale[unit]

    # Generate lamdba/inline function and return
    return lambda t: endo + (epi-endo)*t
