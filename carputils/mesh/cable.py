#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import numpy as np
from carputils.mesh.general import Mesh

class Cable(Mesh):
    """
    Generate a one dimensional mesh of line segments.

    The generated mesh is aligned along the x axis, with y and z set to 0. The
    mesh is defined by three parameters, the start and end x coordinate and the
    node spacing, in mm.

    To generate a mesh from x=0 to x=10, with 0.1mm spacing:

    >>> geom = Cable(0, 10, 0.1)
    >>> geom.generate_carp('mesh/cable')

    At present, fibres are always aligned in the positive x direction.

    For more information on defining mesh regions etc., please see the
    :ref:`mesh_generation` section of the documentation.
    
    Args:
        x0 : float
            The lower x limit of the mesh, in mm
        x1 : float
            The upper x limit of the mesh, in mm
        dx : float, optional
            The node spacing of the mesh, in mm, defaults to 0.1
    """

    def __init__(self, x0, x1, dx=0.1):
        
        Mesh.__init__(self)

        self._x0 = float(x0)
        self._x1 = float(x1)
        self._dx = float(dx)

    def _num_divs(self):
        """
        Return the number of divisions, or line segments, in the mesh.

        Returns:
            int
                The number of line segments/divisions
        """
        return int(np.ceil((self._x1 - self._x0) / self._dx))
    
    def _generate_points(self):
        """
        Generate the node points of the mesh and store internally.
        """

        n_points = self._num_divs() + 1

        x = np.linspace(self._x0, self._x1, n_points)
        y = np.zeros(n_points)
        z = np.zeros(n_points)

        self._ptsarray = np.column_stack((x,y,z))

    def _generate_elements(self):
        """
        Generate the elements of the mesh and store internally.
        """

        divs = self._num_divs()
        elems = np.column_stack((np.arange(divs), np.arange(divs)+1))

        self._mixedelem = {'line': elems}

    def _calc_fibres(self):
        """
        Generate the fibre vectors of the mesh and store internally.
        """
        self._lonarray = np.array([[1.,0.,0.]] * self._num_divs())
