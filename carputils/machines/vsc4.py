#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from carputils.machines.general import BatchPlatform
import os
import math
TEMPLATE = """#!/bin/bash
#
#SBATCH -J {jobID}
#SBATCH -N {nnode}
#SBATCH --ntasks-per-node=48
#SBATCH --ntasks-per-core=1
#SBATCH --output {jobID}.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user {email}
#SBATCH --time {walltime}

# No Hybrid MPI/OpenMP
export OMP_NUM_THREADS=1

# purge modules
module purge

# load user defined bashrc which is assumed to have the correct modules
source {config}

"""

class VSC4(BatchPlatform):
    """
    Run on VSC-4 at the Vienna Scientific Cluster

    Info: http://vsc.ac.at/systems/vsc-4/
    """

    SUBMIT    = 'sbatch'
    if 'MPI_PMI_LIB' in os.environ:
        LAUNCHER  = 'srun'
    else:
        LAUNCHER  = 'mpirun'
    BATCH_EXT = '.slrm'
    SINGLE_CORE = False

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, nproc_job=0, ddt=False, cuda=False,
                     *args, **kwargs):
        if nproc_job == 1:
            cmd = carp_cmd
            SINGLE_CORE = True
        else:
            cmd = [cls.LAUNCHER, '-n', nproc] + carp_cmd

        if ddt:
            # Run with ddt in reverse connection mode
            cmd = ['ddt', '--connect'] + cmd

        return cmd

    @classmethod
    def add_python_launcher(cls, exe_cmd):
        cmd = ['python'] + exe_cmd
        return cmd

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email, config=None):

        nproc = int(nproc)

        # Check sensible number of nodes
        assert nproc % 48 == 0, 'Use a multiple of 48 processes on VSC4'

        nnode = math.ceil(nproc / 48)

        config = config if config else os.path.join(os.environ['HOME'], '.bashrc')
        tmp = TEMPLATE.format(jobID=jobID, nnode=nnode, walltime=walltime,
                              email=email, config=config)
        if 'MPI_PMI_LIB' in os.environ and SINGLE_CORE:
            tmp += f"\nexport I_MPI_PMI_LIBRARY={os.environ['MPI_PMI_LIB']}\n"
        return tmp

    @classmethod
    def polling(cls, polling_opts, nproc, nproc_job, script):
        from carputils.settings import execs

        if polling_opts is None:
            return None

        # Check sensible number of nodes per job
        assert nproc_job <= nproc,\
            'Number of Processes per job has to be smaller than the total '\
            'number of processes!'

        # Get maximal number of tasks per node
        max_tasks = int(48)

        # Add polling file options
        poll_tmp = "runopts=(\n"

        nruns = len(polling_opts)
        for i in range(nruns):
            poll_tmp += '  \"' + \
                        str(polling_opts[i]).replace('\n', '').replace('\r', '') + \
                        '\"\n'

        poll_tmp += ")\n\n"
        poll_tmp += "tasks_to_be_done=${#runopts[@]}  ## total number of tasks\n"
        poll_tmp += "max_tasks={}                     "\
                    "## number of tasks per node\n\n".format(max_tasks)
        poll_tmp += "current_task=0                   ## initialization\n"
        poll_tmp += "running_tasks=0                  ## initialization\n\n"

        poll_tmp += "while (($current_task < $tasks_to_be_done))\n"\
                    "do\n\n"\
                    "    ## count the number of tasks currently running\n"\
                    f"    running_tasks=`ps -C {os.path.basename(str(execs.CARP))} --no-headers | wc -l`\n\n"\
                    "    while (($running_tasks < $max_tasks && "\
                    "${current_task} < ${tasks_to_be_done}))\n"\
                    "    do\n"
        poll_tmp += "        ## get opts\n"\
                    "        opts=${runopts[$current_task]}\n\n" \
                    "        echo \"Current task:     $current_task\"\n" \
                    "        echo \"Tasks to be done: $tasks_to_be_done\"\n" \
                    "        echo \"Running above:    $running_tasks\"\n\n" \
                    "        ## run application\n"
        poll_tmp += "        " + script.rstrip('\n') + "\\\n" \
                    " $opts &\n\n"
        poll_tmp += "        ## increase task number\n"\
                    "        ((current_task++))\n\n"
        poll_tmp += "        ## count the number of tasks currently running\n"\
                    f"        running_tasks=`ps -C {os.path.basename(str(execs.CARP))} --no-headers | wc -l`\n"\
                    "    done\n"\
                    "done\n"\
                    "wait\n"

        return poll_tmp
