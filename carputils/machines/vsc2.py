#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from carputils.machines.general import BatchPlatform

TEMPLATE = """#!/bin/sh -f
#$ -V
#$ -cwd
#$ -j y
#$ -m be
#$ -pe mpich {nproc}
#$ -l h_rt={walltime}
#$ -o {jobID}_{nproc}.$JOB_ID
#$ -N {jobID}_{nproc}

#export I_MPI_DAT_LIBRARY=/usr/lib64/libdat2.so.2
#export OMP_NUM_THREADS=1
#export I_MPI_FABRICS=shm:dapl
#export I_MPI_FALLBACK=0
#export I_MPI_CPUINFO=proc
#export I_MPI_PIN_PROCESSOR_LIST=0-15
#export I_MPI_JOB_FAST_STARTUP=0
"""

class VSC2(BatchPlatform):
    """
    Run on VSC-2 at the Vienna Scientific Cluster

    Info: http://vsc.ac.at/systems/vsc-2/
    """

    SUBMIT    = 'qsub'
    LAUNCHER  = 'mpirun'
    BATCH_EXT = '.pbs'

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):
        return TEMPLATE.format(jobID=jobID, nproc=nproc, walltime=walltime)
