#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from carputils.machines.general import BatchPlatform

TEMPLATE = """#!/bin/bash
#MSUB -r TBunnyC2     # Request name
#MSUB -n {nproc}      # Number of tasks to use
#MSUB -T {walltime}   # Elapsed time limit in seconds
#MSUB -o {jobID}_%I.o # Standard output. %I is the job id
#MSUB -e {jobID}_%I.e # Error output. %I is the job id
#MSUB -A pa1332       # Project ID
#MSUB -q standard     # Choosing standard nodes

set -x
cd $BRIDGE_MSUB_PWD
"""

class Curie(BatchPlatform):
    """
    Run on the Curie supercomputer

    Info: http://www-hpc.cea.fr/en/complexe/tgcc-curie.htm
    """

    SUBMIT    = 'ccc_msub'
    LAUNCHER  = 'ccc_mprun'
    BATCH_EXT = '_Script'

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):
        return TEMPLATE.format(jobID=jobID, nproc=nproc, walltime=walltime)
