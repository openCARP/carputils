#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from carputils.machines.general import BatchPlatform

TEMPLATE = """#!/bin/bash --login
#PBS -N {jobID:.15s}
#PBS -l nodes={nnodes}:ppn={ppn}
#PBS -l walltime={walltime}

SCRATCH_DIR=/usr/sci/cluster/username/$USER/$PBS_JOBID

# make sure the scratch directory is created
mkdir -p $SCRATCH_DIR

# copy datafiles from directory where I typed qsub, to scratch directory
cp -r $PBS_O_WORKDIR/* $SCRATCH_DIR/

#change to the scratch directory
cd $SCRATCH_DIR

# Set the number of threads to 1
#   This prevents any system libraries from automatically
#   using threading.
export OMP_NUM_THREADS=1

"""

class Wopr(BatchPlatform):
    """
    Run jobs on SCI GPU cluster wopr 

    Info: https://internal.sci.utah.edu/cluster 
    """

    SUBMIT    = 'qsub'
    LAUNCHER  = 'mpiexec'
    BATCH_EXT = '.pbs'

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):

        nproc = int(nproc)
        ppn   = 8

        # Check sensible number of nodes
        assert nproc % ppn == 0, 'Use a multiple of 8 processes on WOPR'

        nnodes = int(nproc / ppn)
        
        return TEMPLATE.format(jobID=jobID, nnodes=nnodes, ppn=ppn, walltime=walltime)

    @classmethod
    def footer(cls):
        return "cp -r $SCRATCH_DIR/* $PBS_O_WORKDIR/"
