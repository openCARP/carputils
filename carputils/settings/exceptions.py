#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

class CARPUtilsSettingsError(Exception):
    pass

class CARPUtilsMissingSettingError(CARPUtilsSettingsError):
    def __init__(self, setting, path, *args, **kwargs):
        msg = ('The required setting \'{}\' is missing from your settings file'
               ' (path: {})').format(setting, path)
        Exception.__init__(self, msg)

class CARPUtilsMissingPathError(CARPUtilsSettingsError):
    def __init__(self, attr, path=None, *args, **kwargs):
        msg = 'The path for {} was not found'.format(attr)
        if path is not None:
            msg += ' ({})'.format(path)
        Exception.__init__(self, msg)

class CARPUtilsMissingLicenseError(CARPUtilsSettingsError):
    def __init__(self, module, *args, **kwargs):
        msg =  ('\n\nChosen set of parameters requires the \'{}\' option to be enabled.\n').format(module)
        msg += ('Please recompile openCARP or apply for the missing licence!\n')
        Exception.__init__(self, msg)

class CARPUtilsSettingsWarning(UserWarning):
    pass

