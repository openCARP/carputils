import os
import numpy


def read_lats_all(fname):
    if os.path.exists(fname):
        data_type = [('nod_idx', 'i8'), ('lat', 'f8')]
        data = numpy.genfromtxt(fname, dtype=data_type, delimiter='\t')
    else:
        print('LAT activation file "{}" not found !'.format(fname))
        return None

    nod_idx, lats = data['nod_idx'], data['lat']
    # sort by nodal indices
    si = numpy.argsort(nod_idx)
    nod_idx = nod_idx[si]
    lats = lats[si]
    # rearrange in matrix form
    nodes = numpy.max(nod_idx) + 1
    # determine max number of repeated nodes
    n_rep = numpy.zeros(nodes, dtype=int)
    n_rep[:] = -1

    unique, counts = numpy.unique(nod_idx, return_counts=True)
    for i, nidx in enumerate(unique):
        n_rep[nidx] = counts[i]

    # check activation pattern
    # maximum number of actiations at a single node
    max_acts = int(numpy.max(n_rep))
    # all nodes were at least activated once
    all_acts = numpy.min(n_rep) > -1
    # all nodes were activated equally often
    same_mlt = numpy.all(n_rep == n_rep[0])
    # create matrix nodes x activations
    lat_mat = numpy.zeros((nodes, max_acts))
    # initalize with lat >> than the latest lat detected
    lat_mat[:] = numpy.max(lats) + 1000

    # fill lat matrix, nodal index is matrix line, column is the j-th activation time
    lc = 0
    for i in range(nodes):
        mlt = n_rep[i]
        if mlt > -1:
            for j in range(mlt):
                lat_mat[i, j] = lats[lc]
                lc += 1

        # final sort for ascending activation times
        lat_mat[i, :] = sorted(lat_mat[i, :])

    # now that we are sorted, replace not active nodes with -1 again
    lat_mat[lat_mat > (numpy.max(lats)+1)] = -1
    return lat_mat


def save_lats_all(fname, lat_mat):
    numpy.savetxt(fname, lat_mat)


def convert_lats_all(fname, newfname):
    lat_mat = read_lats_all(fname)
    save_lats_all(newfname, lat_mat)


