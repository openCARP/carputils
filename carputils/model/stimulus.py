#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os
import math
from .general import AbstractModelComponent
from carputils.resources import trace

class Stimulus(AbstractModelComponent):
    """
    Represents a single stimulus argument to openCARP.

    Args:
        name : str, optional
            A short descriptive name for this stimulus
        \**kwargs
            The remaining fields for the stimulus
    """

    PRM_ARRAY  = 'stim'
    PRM_LENGTH = 'num_stim'

    FIELDS = ['name', 'elec.vtx_file', 'elec.vtx_fcn', 'elec.dump_vtx_file', 'elec.p1', 'elec.p0', 
              'elec.geom_type', 'elec.geomID', 'elec.radius', 'ptcl.start', 'ptcl.bcl', 'ptcl.npls', 
              'ptcl.stimlist', 'ptcl.name', 'ptcl.duration', 'pulse.strength', 'pulse.s2', 
              'pulse.tau_edge', 'pulse.tilt_ampl', 'pulse.tilt_time', 'pulse.file', 'pulse.shape', 
              'pulse.name', 'pulse.tau_plateau', 'pulse.bias', 'crct.type', 'crct.total_current', 
              'crct.balance', 'crct.total_current', 'elec.domain']

    def __init__(self, name=None, **kwargs):
        super(Stimulus, self).__init__(name=name, **kwargs)
    
    def opts_formatted(self, index):
        return super(Stimulus, self).opts_formatted(index)

            
    @classmethod
    def forced_foot(cls, act_sequence=None, pulse_file=None):
        """
        Create a forced foot stimulus using a pulse file.
    
        Args:
            act_sequence : str, optional
                Path of the activation sequence to use for the stimulus - if not
                given will be computed
            pulse_file : str, optional
                Path of the trace to use to prescribe the foot, without .trc
                extension - a sensible default is used if not given
        """
        
        if pulse_file is None:
            pulse_file = trace.path('standard_ap_foot')
    
        if not os.path.exists(pulse_file + '.trc'):
            msg = ('pulse file {}.trc does not exist, remember to omit the '
                   '.trc file extension').format(pulse_file)
            raise ValueError(msg)

        kwargs = {'pulse.file': pulse_file}
        # if act_sequence is not None:
        #     kwargs['data_file'] = act_sequence
    
        return cls('forced_foot', stimtype=8, **kwargs)
    
    @classmethod
    def forced_foot_exponential(cls, act_sequence=None):
        """
        Create a forced foot stimulus with an exponential shape.
    
        Args:
            act_sequence : str, optional
                Path of the activation sequence to use for the stimulus - if not
                given will be computed
        """
    
        # Foot settings
        Vrest = -85
        Vth   = -55
        duration = 3
        tau_foot = 3
    
        # Get up needed parameters
        kwargs = {}
        kwargs['pulse.strength'] = Vth - Vrest
        kwargs['pulse.bias'] = 2 * Vrest - Vth
        kwargs['ptcl.duration'] = 3
        kwargs['pulse.tau_plateau'] = tau_foot / math.log(2)
        kwargs['pulse.tau_edge'] = 0.0

        # if act_sequence is not None:
        #     kwargs['data_file'] = act_sequence
    
        # Initialise and return stimulus
        return cls('forced_foot', stimtype=8, **kwargs)
