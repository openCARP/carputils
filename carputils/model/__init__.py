#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

from .general import optionlist
from .general import convert_param_dict
from .general import Modifier, AddModifier, MultModifier

from . import ionic
from . import activetension
from . import mechanics
from . import induceReentry
from . import prepace

from .stimulus import Stimulus
from .conductivity import ConductivityRegion
from .eikonal import EikonalRegion

# Make aliases for compactness
GRegion  = ConductivityRegion
EkRegion = EikonalRegion
