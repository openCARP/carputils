#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Code to automatically generate model component classes from the CARP source.
"""

import os
import re
import sys
import subprocess

from carputils import tools
from carputils import settings
from collections import OrderedDict


isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False

HEADER = "\
#\n\
# This file is part of openCARP\n\
# (see https://www.openCARP.org).\n\
#\n\
# The openCARP project licenses this file to you under the\n\
# Apache License, Version 2.0 (the \"License\");\n\
# you may not use this file except in compliance\n\
# with the License.  You may obtain a copy of the License at\n\
#\n\
#   http://www.apache.org/licenses/LICENSE-2.0\n\
#\n\
# Unless required by applicable law or agreed to in writing,\n\
# software distributed under the License is distributed on an\n\
# \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY\n\
# KIND, either express or implied.  See the License for the\n\
# specific language governing permissions and limitations\n\
# under the License.\n\
#\n"

NAMEMAP = {'Neohooke':        'NeoHookean',
           'StVK':            'StVenantKirchhoff',
           'Artery':          'HolzapfelArterial',
           'Myocardium':      'HolzapfelOgden',
           'NHSstress':       'NHS',
           'LandStress':      'Land',
           'LandHumanStress': 'LandHuman',
           'FxStress':        'Fx',
           'TanhStress':      'Tanh',
           'NPStress':        'NP'}

DESCRIPTION_NAMEMAP = {'Neohooke':   'neo-Hookean',
                       'NHSstress':  'Niederer-Hunter-Smith',
                       'NPStress':   'Nash-Panfilov',
                       'Muscon':     'Hunter-McCulloch-ter Keurs',
                       'LandStress': 'Land'}

PARAM_TYPES = {'name': 'str',
               'IDs':  'list of int'}

PARAM_DESC = {'name': 'The name of the model material region',
              'IDs':  'The element IDs to be included in this material region',
              'kappa': 'Value of the incompressibility penalty'}


def wrap(text, linelen=80, pad=0):
    """
    Wrap lines of text.
    """

    space = linelen - pad
    lines = ['']

    for word in text.split():
        if len(lines[-1]) + len(word) + 1 > space:
            lines.append('')
        if len(lines[-1]) > 0:
            lines[-1] += ' '
        lines[-1] += word

    return ' ' * pad + ('\n' + ' ' * pad).join(lines) + '\n'


def param_sort(keys, kappa_first=True):
    order = sorted(keys, key=lambda s: s.lower())
    if 'kappa' in order:
        order.pop(order.index('kappa'))
        if kappa_first:
            order = ['kappa'] + order
        else:
            order += ['kappa']
    return order


def param_iterate(params):
    for key in param_sort(params.keys(), False):
        yield key, params[key]


def write_param_desc(name, default=None):

    # Get description
    if name in PARAM_DESC:
        description = PARAM_DESC[name]
    else:
        description = 'Value of {}'.format(name)

    if default is not None:
        description += ', defaults to {}'.format(default)

    desc = '    {} : {}\n'.format(name, PARAM_TYPES.get(name, 'float, optional'))
    desc += wrap(description, pad=8)

    return desc


def write_class(name, parent, description, modelid=None,
                plugin=None, shortname=None, params={},
                region=True):

    code = 'class {}({}):\n'.format(name, parent)
    code += '    """\n'
    code += wrap(description, pad=4)
    code += '\n'
    code += '    Parameters\n'
    code += '    ----------\n'
    if region:
        code += write_param_desc('IDs')
        code += write_param_desc('name')
    for name, default in param_iterate(params):
        code += write_param_desc(name, default)
    code += '    """\n'

    if shortname is not None:
        code += "    SHORTNAME = '{}'\n".format(shortname)

    if modelid is not None:
        if isinstance(modelid, int):
            code += '    MODELID = {}\n'.format(modelid)
        else:
            code += "    MODELID = '{}'\n".format(modelid)

    if plugin is not None:
        code += "    PLUGIN = '{}'\n".format(plugin)

    # Add param names
    code += '    PARAMETERS = {'
    parts = [repr(s) + ': ' + repr(k) + ', ' for s, k in param_iterate(params)]

    pad = len('PARAMETERS = {') + 4
    space = 80 - pad

    for p in parts:
        if len(p) > space:
            code += '\n' + ' ' * pad
            space = 80 - pad
        code += p + ' '
        space -= len(p) + 1

    # Drop last comma and space
    if len(params) > 0:
        code = code[:-3]

    code += '}\n'

    return code


def write_maps(mapping):

    maxlen = max([len(k) for k in mapping.keys()])
    key_tpl = "['{}']"
    line_tpl = 'MAPPING{:<' + str(maxlen + 4) + 's} = {}\n'

    code = 'MAPPING = _OrderedDict()\n'
    for key, cls in mapping.items():
        code += line_tpl.format(key_tpl.format(key), cls)

    code += '\n\n'
    code += 'def keys():\n'
    code += '    """\n'
    code += '    Return a list of string keys to be used with :func:`get`\n'
    code += '    """\n'
    code += '    return MAPPING.keys()\n'
    code += '\n\n'
    code += 'def get(*args, **kwargs):\n'
    code += '    """\n'
    code += '    Get a class from its key string from :func:`keys`\n'
    code += '    """\n'
    code += '    return MAPPING.get(*args, **kwargs)\n'

    return code


def write_summary(modeltype, modelattr=[]):

    modelattrstr = ', '.join(map(lambda o: '\'{}\''.format(str(o).upper()), modelattr))
    code  = 'def summary(indent=2, ncolumns=4, padding=2):\n'
    code += '    """\n'
    code += '    Return a string summarizing the models and their parameters\n'
    code += '    """\n'
    code += '    modelattr = [{}]\n'.format(modelattrstr)
    code += '    maxattrlen = max(map(len, modelattr+[\'PARAMETERS\']))\n'
    code += '    summarystr = \'{} MODELS\\n\\n\'\n'.format(modeltype.upper())
    code += '    indentstr = \' \'*indent\n'
    if isPy2:
        code += '    for name, model in MAPPING.iteritems():\n'
    else:
        code += '    for name, model in MAPPING.items():\n'
    code += '        summarystr += indentstr+\'{}\\n\'.format(name)\n'
    code += '        for attr in modelattr:\n'
    code += '            obj = getattr(model, attr, None)\n'
    code += '            if obj is None:\n'
    code += '                continue\n'
    code += '            summarystr += indentstr*2+\'{}: {}\\n\'.format(attr.lower().ljust(maxattrlen), str(obj))\n'
    code += '        attr = \'PARAMETERS\'\n'
    code += '        obj = getattr(model, attr, None)\n'
    code += '        if obj is not None and len(obj) > 0:\n'
    code += '            tablestr = stringtable(obj, ncolumns, padding, rowprefix=\' \'*(maxattrlen+2*indent+2))\n'
    code += '            summarystr += indentstr*2+attr.lower().ljust(maxattrlen)+\': \'+tablestr[len(attr)+2*indent+2:]\n'
    code += '        summarystr += \'\\n\'\n'
    code += '    return summarystr\n'

    return code


def mechanics_materials(elasticity):

    elasticity_help = os.path.join(elasticity, 'bin', 'elasticity_help')
    cmd = [elasticity_help, '--mat-params']
    elasticity_help_output = subprocess.check_output(cmd).decode('utf-8')
    materials = map(lambda s: s.strip(), elasticity_help_output.split('\n'))

    replacements = (('Reduced', 'Red'),
                    ('Modified', 'Mod'),
                    ('Compressible', 'Comp'),
                    ('Dispersion', 'Disp'),
                    ('Material', ''))

    code = '\n'
    code += 'from .general import AbstractMechanicsMaterial'
    code += ' as _AbstractMechanicsMaterial\n'
    code += 'from collections import OrderedDict as _OrderedDict\n'
    code += 'from carputils.format import stringtable\n'

    code += '\n\n'
    code += 'def grid_id():\n'
    code += '    """\n'
    code += '    Return mechanics grid ID\n'
    code += '    """\n'
    code += '    grid_id = 8  # grid ID\n'
    code += '    return grid_id\n'

    desc_tpl = 'Describes the {} mechanics material'

    mapping = OrderedDict()

    for mat in materials:
        if not len(mat) > 0:
            continue

        label, index, param_str, name = mat.split(';')

        # convert index string to integer
        index = int(index)

        # convert label to a short label
        short_label = label
        for rep in replacements:
            short_label = short_label.replace(rep[0], rep[1])
        short_label = short_label.lower()

        # split params
        params = []
        for param in param_str.split(','):
            param_name, param_value = param.split('=')
            params.append((param_name, float(param_value)))
        params = dict(params)

        code += '\n\n'
        code += write_class(label, '_AbstractMechanicsMaterial',
                            desc_tpl.format(name), index,
                            shortname=short_label, params=params)

        mapping[short_label] = label

    code += '\n\n'
    code += write_maps(mapping)
    code += '\n\n'
    code += write_summary('MATERIAL', ['SHORTNAME', 'MODELID'])

    return code


def tension_plugin(name, directory):
    header_filename = os.path.join(directory, '{}.h'.format(name))
    with open(header_filename) as fp:
        for line in fp:
            line = line.strip()
            if line.startswith('#define') and 'Tension_DATA_FLAG' in line:
                return True
    return False


def limpet_read_imps(limpetdir):
    """
    Append LIMPET path ahead of other PATH includes.
    Matters across different CARP versions.
    """
    sys.path.insert(0, limpetdir)
    #print(sys.path)
    import common as limpetcommon

    from carputils.testing.test import temporary_wkdir
    with temporary_wkdir(limpetdir):
        return limpetcommon.readIMPs('OFF')


def limpet_params(name, directory):
    """
    Scrub most of the parmeters and default values from the source code.
    """

    header_filename = os.path.join(directory, '{}.h'.format(name))

    params = {}
    in_params = False

    start = re.compile(r'typedef\s+struct\s+\S+_params')
    param = re.compile(r'\s*\S+\s+(\S+)\s*;')
    end = re.compile(r'\}\s*(\S+_Params)')

    struct_name = None

    with open(header_filename) as fp:
        for line in fp:
            if in_params:
                if end.search(line) is not None:
                    struct_name = end.search(line).group(1)
                    break
                #if 'UNMODIFIABLE BELOW HERE' in line:
                #    break
                match = param.search(line)
                if match is not None:
                    params[match.group(1)] = None
            if start.search(line) is not None:
                in_params = True
        else:
            raise Exception()

    assert struct_name is not None, 'struct name was not found'

    body_filename = os.path.join(directory, '{}.cc'.format(name))

    definitions = {}
    define = re.compile(r'#define\s+(\S+)\s+(\S+)')

    structstart = re.compile(r'\s*' + struct_name + r'\s*\*\s*(\S+)\s*[=;]')
    instancevar = None

    with open(body_filename) as fp:

        for line in fp:

            # Collect definitions
            match = define.match(line)
            if match is not None:
                try:
                    definitions[match.group(1)] = float(match.group(2))
                except ValueError:
                    pass

            # Find the instance of the param struct
            match = structstart.match(line)
            if match is not None:
                instance = match.group(1)
                instancevar = re.compile(instance + r'->(\S+)\s*\=\s*(\S+)\s*;')

            if instancevar is not None:
                match = instancevar.search(line)
                if match is not None:
                    name = match.group(1)
                    val = match.group(2)
                    if val in definitions:
                        params[name] = definitions[val]
                    else:
                        try:
                            params[name] = float(val)
                        except ValueError:
                            pass

    return params


def ionic_models(limpetdir):

    models = limpet_read_imps(limpetdir)[0]

    mapping = OrderedDict()

    code = '\n'
    code += 'from .general import AbstractIonicModel as _AbstractIonicModel\n'
    code += 'from collections import OrderedDict as _OrderedDict\n'
    code += 'from carputils.format import stringtable\n'

    # Add PASSIVE manually
    desc = 'Describes the passive ionic model configuration'
    code += '\n\n'
    code += write_class('PassiveIonicRegion', '_AbstractIonicModel', desc,
                        modelid='PASSIVE', shortname='passive')
    mapping['passive'] = 'PassiveIonicRegion'

    for name in models:

        # Get list of parameters
        params = limpet_params(name, limpetdir)

        tpl = 'Describes the {} ionic model'
        desc = tpl.format(DESCRIPTION_NAMEMAP.get(name, name))

        code += '\n\n'
        class_stub = NAMEMAP.get(name, name)
        code += write_class(class_stub + 'IonicModel',
                            '_AbstractIonicModel',
                            desc, modelid=name, shortname=class_stub.lower(),
                            params=params)

        mapping[class_stub.lower()] = class_stub + 'IonicModel'

    code += '\n\n'
    code += write_maps(mapping)
    code += '\n\n'
    code += write_summary('IONIC', ['SHORTNAME', 'MODELID'])

    return code


def active_tension_models(limpetdir):

    plugs = limpet_read_imps(limpetdir)[1]

    mapping = OrderedDict()

    code = '\n'
    code += 'from .general import AbstractPlugin as _AbstractPlugin, NoPlugin\n'
    code += 'from collections import OrderedDict as _OrderedDict\n'
    code += 'from carputils.format import stringtable\n'

    for name in sorted(plugs, key=lambda s:s.lower()):

        # Filter out non-tension plugins
        if not tension_plugin(name, limpetdir):
            continue

        # Get list of parameters
        params = limpet_params(name, limpetdir)

        tpl = 'Describes the {} active stress model'
        desc = tpl.format(DESCRIPTION_NAMEMAP.get(name, name))

        code += '\n\n'
        class_stub = NAMEMAP.get(name, name)
        code += write_class(class_stub + 'Stress',
                            '_AbstractPlugin',
                            desc, plugin=name, shortname=class_stub.lower(),
                            params=params, region=False)

        mapping[class_stub.lower()] = class_stub + 'Stress'

    mapping['none'] = 'NoPlugin'

    code += '\n\n'
    code += write_maps(mapping)
    code += '\n\n'
    code += write_summary('STRESS', ['SHORTNAME', 'PLUGIN'])

    return code


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('carp_source',
                        help='the root of the openCARP source directory to utilise')
    args = parser.parse_args()

    model_dir = os.path.dirname(__file__)

    # Make sure settings populated
    tools.standard_parser().parse_args(['--silent'])

    elasticity = os.path.join(args.carp_source, 'elasticity')
    with open(os.path.join(model_dir, 'mechanics.py'), 'w') as fp:
        fp.write(HEADER)
        fp.write(mechanics_materials(elasticity))

    limpet = os.path.join(args.carp_source, 'LIMPET')
    with open(os.path.join(model_dir, 'ionic.py'), 'w') as fp:
        fp.write(HEADER)
        fp.write(ionic_models(limpet))

    with open(os.path.join(model_dir, 'activetension.py'), 'w') as fp:
        fp.write(HEADER)
        fp.write(active_tension_models(limpet))

