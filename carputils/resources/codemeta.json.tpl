{
  "@context": [
    "https://doi.org/10.5063/schema/codemeta-2.0",
    "http://schema.org"
  ],
  "@type": "SoftwareSourceCode",

  "name": "",
  "description": "",
  "author": [
    {
      "id": "",
      "type": "Person",
      "familyName": "",
      "givenName": "",
      "email": ""
    }
  ],
  "dateModified": "",
  "codeRepository": "",
  "copyrightHolder": [
    {
      "type": "Person",
      "familyName": "",
      "givenName": ""
    }
  ],
  "publisher": {
    "@type": "Organization",
    "name": "openCARP"
  },
  
  "applicationCategory": "Simulation code",
  "programmingLanguage": "Python",
  "keywords": [
    "cardiac electrophysiology",
    "openCARP experiment",
    "simulation software",
    "in-silico medicine",
    "computational cardiology",
    "Computer simulation",
    "Biomedical engineering",
    "Cardiology",
    "LifeScience",
    "ComputerScience",
    "Medicine"
  ],
  "version": "v1.0",
  "license": {
    "@type": "CreativeWork",
    "name": "Apache-2.0",
    "url": "https://git.opencarp.org/openCARP/FACILE-RS/-/blob/master/LICENSE"
  },
  "funding": [
    {
      "@type": "Grant",
      "name": "Numerical modeling of cardiac electrophysiology at the cellular scale (MICRCOARD-2)",
      "identifier": 101172576,
      "url": "https://microcard.eu",
      "funder": {
        "@type": "Organization",
        "@id": "https://ror.org/00k4n6c32",
        "name": "European High-Performance Computing Joint Undertaking EuroHPC (JU)"
      }
    }
  ],
  "isPartOf": {
    "@type": "CreativeWork",
    "name": "openCARP",
    "@id": "https://doi.org/10.1016/j.cmpb.2021.106223"
  }
}