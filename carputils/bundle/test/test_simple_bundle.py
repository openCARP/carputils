import sys
import os.path
import subprocess
from zipfile import ZipFile


def test_simple_bundle(monkeypatch, tmp_path):
    """
    Test bundle creation when using the function bundle_include to include external files.
    """
    # Path to the carputils test experiment directory
    test_sim_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'resources', 'sim_simple'))
    monkeypatch.chdir(tmp_path)
    # carputils script location
    run_script = os.path.join(test_sim_dir, 'run.py')
    # Execute "python run.py --bundle simple" in a subshell
    subprocess.run([sys.executable, run_script, '--bundle', 'simple'], check=True)
    # Extract the bundle
    bundle_dir = tmp_path / 'simple_bundle'
    # Check that the bundle contains all expected files
    monkeypatch.chdir(bundle_dir)
    assert os.path.exists('codemeta.json')
    assert os.path.exists('run.py')
    assert os.path.exists('external/simple.par')
    assert os.path.exists('external/simple.mshz')
    assert os.path.exists('external/dummy.txt')
    # Ensure the script in the bundle can be run
    subprocess.run([sys.executable, 'run.py'], check=True)


def test_bundleinclude(monkeypatch, tmp_path):
    """
    Test bundle creation when using a .bundleinclude file to include files within the bundle.
    """
    test_sim_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'resources', 'sim_simple_bundleinclude'))
    monkeypatch.chdir(tmp_path)
    # carputils script location
    run_script = os.path.join(test_sim_dir, 'run.py')
    # Execute "python run.py --bundle simple" in a subshell
    subprocess.run([sys.executable, run_script, '--bundle', 'simple'], check=True)
    # Extract the bundle
    bundle_dir = tmp_path / 'simple_bundle'
    # Check that the bundle contains all expected files
    monkeypatch.chdir(bundle_dir)
    assert os.path.exists('codemeta.json')
    assert os.path.exists('run.py')
    assert os.path.exists('simple.par')
    assert os.path.exists('view/simple.mshz')
    # Ensure the script in the bundle can be run
    subprocess.run([sys.executable, 'run.py'], check=True)
