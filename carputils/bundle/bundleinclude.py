"""
Manages .bundleinclude file, which allows to explicitely include files and folders to bundles.
The items listed in .bundleinclude must be contained in the experiment directory or one of its subdirectories.
"""

from glob import glob
import os.path
from shutil import copy2

from carputils.bundle.path_utils import is_subdirectory
from carputils.bundle import bundleio


def copy_bundleinclude(exp_dir, bundle_dir):
    """
    Copy files and folders in the bundle according to the '.bundleinclude' file contained in exp_dir.
    Files and folders can be provided using patterns, as in the glob module.
    Only files which are contained in the experiment directory or one of its subdirectories are copied.

    Args:
        exp_dir : str
            the path to the original experiment directory.
        bundle_dir : str
            the path to the bundle directory.
    """
    try:
        file = open(os.path.join(exp_dir, '.bundleinclude'), 'r')
    except FileNotFoundError:
        print('Info: no .bundleinclude file found.')
        return
    for line in file:
        filenames = parse_bundleinclude_line(line, exp_dir)
        for f in filenames:
            rel_f = os.path.relpath(f, start=exp_dir)
            abs_f = os.path.abspath(f)
            # copy file in bundle
            if os.path.isdir(abs_f):
                bundleio.copy_folder_in_bundle(rel_f, exp_dir, bundle_dir)
            if os.path.isfile(abs_f):
                dest_f = os.path.join(bundle_dir, rel_f)
                os.makedirs(os.path.dirname(dest_f), exist_ok=True)
                copy2(abs_f, dest_f)
    file.close()

import contextlib


@contextlib.contextmanager
def temporary_cd(x):
    """
    Context manager to temporarily change the current working directory.
    """
    d = os.getcwd()
    os.chdir(x)
    try:
        yield
    finally:
        os.chdir(d)


def parse_bundleinclude_line(line, exp_dir):
    """
    Parses a line of a .bundleinclude file.

    Args:
        line : str
            a line of a .bundleinclude file. It must contain a pattern to match files and folders within exp_dir.
            If the line is empty, an empty list is returned.
    Returns:
        A list of absolute paths corresponding to files and folders matching the pattern.
    """
    line = line.strip()
    if line == '':
        return []
    
    with temporary_cd(exp_dir):
        files = glob(line)
    if not files:
        print(f'Warning: .bundleinclude: no file found for pattern {line}.')
    res_files = []
    for f in files:
        if os.path.isabs(f):
            abs_f = os.path.abspath(f)
        else:
            abs_f = os.path.abspath(os.path.join(exp_dir, f))
        if is_subdirectory(abs_f, exp_dir):
            res_files.append(abs_f)
        else:
            print(f'Warning: .bundleinclude: file {f} not copied in the bundle because it is not in the experiment directory.')
    return res_files
