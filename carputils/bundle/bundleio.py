#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os
from shutil import copy2, rmtree
import zipfile
from zipfile import ZipFile

from carputils import clean
from carputils import settings
from carputils.bundle import metadata


def load_pscript(exp_file):
    """
    Reads and returns the content of a script.

    Args:
        exp_file : str
            path to the input file
    Returns:
        string containing the content of the input file
    """
    with open(exp_file, 'r') as f_src:
        s = f_src.read()
        return s


def write_pscript(pscript, pscript_name, bundle_dir):
    """
    Copy the protocol script to the experiment bundle folder

    Args:
        pscript : str
            string to write in the file
        pscript_name : str
            name of the file to write
        bundle_dir : str
            path to the bundle folder
    """
    dest_path = os.path.join(bundle_dir, pscript_name)
    f_dest = open(dest_path, 'w', encoding='utf8')
    f_dest.write(pscript)
    f_dest.close()
    os.chmod(dest_path, 0o755)


def write_metadata_file(metadata_s, bundle_dir):
    """
    Copy the metadata file into the bundle.

    Args:
        metadata_s : str
            string containing the content of the yml metadata file
        bundle_dir : str
            path to the bundle folder
    """
    f_dest = open(os.path.join(bundle_dir, metadata.METADATA_FILE), 'w')
    f_dest.write(metadata_s)
    f_dest.close()


def copy_folder_in_bundle(folder_name, exp_dir, bundle_dir):
    """
    Copy a folder from the experiment folder to the bundle folder

    Args:
        folder_name : str
            name of the folder to copy (path must be relative to exp_dir)
        exp_dir : str
            absolute path to experiment directory
        bundle_dir : str
            absolute path to bundle directory
    """
    # Path to the source folder
    src_folder = os.path.join(exp_dir, folder_name)
    if os.path.isdir(src_folder):
        # Create destination folder
        bundle_folder = os.path.join(bundle_dir, folder_name)
        os.makedirs(bundle_folder, exist_ok=True)
        # Recursively copy all subfolders and files
        for folder, subfolders, filenames in os.walk(src_folder):
            # Relative path in the destination folder
            folder_in_bundle = os.path.relpath(folder, src_folder)
            # Create subfolders of current folder
            for subfolder in subfolders:
                subfolder_in_bundle = os.path.join(folder_in_bundle, subfolder)
                os.mkdir(os.path.join(bundle_folder, subfolder_in_bundle))
            # Copy files in current folder
            for filename in filenames:
                copy2(os.path.join(folder, filename),
                      os.path.join(bundle_folder, folder_in_bundle))


def copy_mesh_dir_in_bundle(exp_name, bundle_dir):
    """
    Copy the folder MESH_DIR/exp_name/ (configured in settings.yaml) in the bundle folder if it exists.
    Can erase mesh data already put in the bundle.

    Args:
        exp_name : str
            name of the experiment
        bundle_dir : str
            directory of the bundle folder
    """
    mesh_dir = os.path.join(settings.config.MESH_DIR, exp_name)
    mesh_dir_bundle = os.path.join(bundle_dir, os.path.join('Mesh', exp_name))
    if os.path.exists(mesh_dir):
        os.makedirs(mesh_dir_bundle, exist_ok=True)
        # Recursively copy all subfolders and files
        for folder, subfolders, filenames in os.walk(mesh_dir):
            # Relative path in the destination folder
            folder_in_bundle = os.path.relpath(folder, mesh_dir)
            # Create subfolders of current folder
            for subfolder in subfolders:
                subfolder_in_bundle = os.path.join(folder_in_bundle, subfolder)
                os.makedirs(os.path.join(mesh_dir_bundle, subfolder_in_bundle), exist_ok=True)
            # Copy files in current folder
            for filename in filenames:
                copy2(os.path.join(folder, filename),
                      os.path.join(mesh_dir_bundle, folder_in_bundle))


def create_zip(dir_path):
    """
    Creates a zip archive of the given directory, at the same location

    Args:
        dir_path : str
            path to the directory to archive
    """
    # Create path to the zip archive
    zip_path = '{}.zip'.format(dir_path)
    # Check if the file already exists
    clean.overwrite_check(zip_path)
    # Create the archive
    with ZipFile(zip_path, 'w') as zipBundle:
        for folder_abspath, subfolders, filenames in os.walk(dir_path):
            folder_name = os.path.relpath(folder_abspath, dir_path)
            # Case where a folder is empty
            if not filenames:
                zipBundle.write(folder_abspath, folder_name, compress_type=zipfile.ZIP_STORED)
            for filename in filenames:
                file_abspath = os.path.join(folder_abspath, filename)
                zipfile_relpath = os.path.join(folder_name, filename)
                zipBundle.write(file_abspath, zipfile_relpath)
