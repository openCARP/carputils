#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import json
import os
from os import path
import sys
import warnings 
from warnings import warn
from collections import OrderedDict
from datetime import date

from carputils import settings
from carputils import resources

"""
This file contains functions allowing to create a metadata file for the bundle.
"""


# Cleaner warning messages
def _warning(message, category = UserWarning, filename = '', lineno = -1, file=None,  line=None):
    print('Warning: '+ str(message))
warnings.showwarning = _warning


METADATA_FILE = 'codemeta.json'
METADATA_TPL_FILE = 'codemeta.json.tpl'


def load_metadata_file(exp_dir, bundle_dir):
    """
    Creates an object containing metadata provided by the user in the
    file exp_dir/codemeta.json, or in bundle_dir/codemeta.json (in this order) and merge it with the metadata template.
    If these files don't exist, metadata template is loaded.

    Args:
        exp_dir : str
            directory where to find the metadata file.
    Returns:
        An object containing metadata.
    """
    # Load metadata template
    content = resources.load(METADATA_TPL_FILE)
    metadata_obj = json.loads(content)
    # Merge user metadata file with it if it exists
    if path.exists(path.join(exp_dir, METADATA_FILE)):
        with open(METADATA_FILE) as fp:
            metadata_user = json.load(fp)
            metadata_obj.update(metadata_user)
        check_critical_fields(metadata_obj)
    elif path.exists(path.join(bundle_dir, METADATA_FILE)):
        with open(path.join(bundle_dir, METADATA_FILE)) as fp:
            metadata_user = json.load(fp)
            metadata_obj.update(metadata_user)
        check_critical_fields(metadata_obj)
    else:
        warn("""You don't provide any metadata for your experiment.
        An autopopulated {} file was created in the bundle, but you should complete it.
        In the future, please consider creating a metadata file in the current directory by
        running {}, and filling it. 
        This file will then be automatically added in the bundles you create.""".format(METADATA_FILE, settings.execs.GENERATEMETADATA))
    return metadata_obj


def check_critical_fields(metadata_obj):
    """
    Raise warnings if metadata template fields were not filled by the user.
    """
    if metadata_obj['name'] is None:
        warn(f"Please set up the 'name' field in {METADATA_FILE}")


def autopopulate_template(metadata_obj):
    """
    Automatically fills some fields in metadata object.

    Args:
        metadata_obj :
            object containing metadata
    Returns:
        Modified metadata
    """
    metadata_obj = autopopulate_authors_names(metadata_obj)
    metadata_obj = autopopulate_date(metadata_obj)
    return metadata_obj


def autopopulate_authors_names(metadata_obj):
    """
    Create full name of the authors from 'given_name' and 'family_name' fields.
    If no name is given for the first author, it is autopopulated with the user
    data in carputils settings file.
    It also fills the 'rights_holder' field with the names of the authors.

    Args:
        metadata_obj :
            object containing metadata
    Returns:
        Modified metadata
    """
    authors = metadata_obj.get('author', [])
    if not isinstance(authors, list):
        authors = [authors]
    if not authors:
        authors.append({
            "type": "Person"
        })
    counter = 0
    for author in authors:
        name = author.get('name', '')
        if name == '':
            if 'givenName' in author:
                if author['givenName']:
                    name += author['givenName'] + ' '
            if 'familyName' in author:
                if author['familyName']:
                    name += author['familyName']
            author['name'] = name
        # If the name of the first author is not provided, it is autopopulated
        if counter == 0 and name == '':
            warn(f"Please add an author to the 'authors' field in {METADATA_FILE}")
            author['name'] = settings.config['NAME'] if settings.config['NAME'] is not None else 'Your Name'
            guess_split = author['name'].split(maxsplit=1)
            author['givenName'] = guess_split[0]
            author['familyName'] = guess_split[1] if len(guess_split) > 1 else ''
            author['id'] = f"https://orcid.org/{settings.config['ORCID']}" if settings.config['ORCID'] is not None else '0000-0000-0000-0000'
            author['email'] = settings.config['EMAIL'] if settings.config['EMAIL'] is not None else 'your.name@example.com'
        counter += 1
    # Fill the rights_holder field with a string containing the names of the authors
    copyright_holder = metadata_obj.get('copyrightHolder', [])
    if not isinstance(copyright_holder, list):
        copyright_holder = [copyright_holder]
    if not copyright_holder:
        copyright_holder.append({
            "type": "Person",
            "familyName": "",
            "givenName": ""
        })
    if not copyright_holder[0].get('familyName', '') \
        or (copyright_holder[0].get('familyName') == 'Name' and copyright_holder[0].get('givenName') == 'Your'):
        copyright_holder[0] = {
            "type": "Person",
            "familyName": authors[0]['familyName'],
            "givenName": authors[0]['givenName']
        }
    return metadata_obj


def autopopulate_date(metadata_obj):
    """
    Fills 'date' and 'publication_year' fields in metadata.

    Args:
        metadata_obj :
            object containing metadata
    Returns:
        Modified metadata
    """
    today = date.today()
    # YYYY-MM-DD
    metadata_obj['dateModified'] = today.strftime("%Y-%m-%d")
    return metadata_obj


def add_env_metadata(metadata_obj):
    """
    Adds to an existing metadata object the environment metadata included by
    default in the bundle.

    Args:
        metadata_obj :
            an object containing metadata.
    Returns:
        The updated metadata object.
    """
    # Add information about the runtime environment used to create the bundle
    pyinfo = '{}.{}.{}'.format(sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
    metadata_obj['runtimePlatform'] = f'Python {pyinfo}, openCARP@{get_opencarp_git_hash()}'
    # Add the latest openCARP version as software requirement if not provided in the metadata
    # Initialize the softwareRequirements field
    if not metadata_obj.get('softwareRequirements', None):
        metadata_obj['softwareRequirements'] = []
    if not isinstance(metadata_obj['softwareRequirements'], list):
        metadata_obj['softwareRequirements'] = [metadata_obj['softwareRequirements']]
    # Check if an openCARP version is already in the metadata
    opencarp_version_provided = False
    for software in metadata_obj['softwareRequirements']:
        if software.get('name', '') == 'openCARP' and software.get('version', '').startswith('v'):
            opencarp_version_provided = True
            break
    # Add the latest openCARP version as a software requirement if not provided
    if not opencarp_version_provided:
        openCARP_version = get_latest_opencarp_version()
        if not openCARP_version:
            openCARP_version = 'latest'
            print('Warning: Please specify a valid openCARP version in the field "softwareRequirements" of the metadata file.')
        else:
            print(f'Warning: In the metadata file, the latest openCARP version ({openCARP_version}) has been set as a software requirement. '\
                  'Please update it if you want to use a different version.')
        metadata_obj['softwareRequirements'].append(
            {
                "name": "openCARP",
                "version": openCARP_version,
            })
    return metadata_obj


def add_exp_name(metadata_obj, exp_name):
    """
    Adds the experiment name to the metadata object if not defined in metadata.

    Args:
        metadata_obj :
            the object containing metadata
    Returns:
        The updated metadata object
    """
    if not metadata_obj.get('name', ''):
        metadata_obj['name'] = exp_name
    return metadata_obj


def add_command_line(metadata_obj, exp_filename):
    """
    Adds the command line that should be used to run the same experiment
    to a metadata object.

    Args:
        metadata_obj :
            Object containng metadata
    Returns:
        Updated metadata object
    """
    # Get the parameters used for creating the bundle
    cl_parameters = sys.argv[1:]
    # Remove the options related to bundling from the command line
    rm_indices = []
    for ind, param in enumerate(cl_parameters):
        if param.startswith('--bundle') or (param.startswith('--') and param.endswith('bundle')):
            rm_indices.append(ind)
            if len(cl_parameters) > ind+1 and not cl_parameters[ind+1].startswith('-'):
                rm_indices.append(ind+1)
    for ind in reversed(rm_indices):
        del cl_parameters[ind]
    # Create the command line
    run_cmd = exp_filename
    if cl_parameters:
        run_cmd += ' ' + ' '.join(cl_parameters)
    # Add the command line to the metadata
    metadata_obj['softwareHelp'] = metadata_obj.get('softwareHelp', [])
    if not isinstance(metadata_obj['softwareHelp'], list):
        metadata_obj['softwareHelp'] = [metadata_obj['softwareHelp']]
    added = False
    for software_help in metadata_obj['softwareHelp']:
        if software_help.get('type', '') == 'HowTo':
            if 'step' in software_help:
                software_help['step'] = run_cmd
                added = True
            break
    if not added:
        metadata_obj['softwareHelp'].append({
            'type': 'HowTo',
            'step': run_cmd
        })
    return metadata_obj


def analyze_metadata(metadata_obj):
    """
    Analyzes the metadata and trigger warnings in case some critical metadata is missing.

    Args:
        metadata_obj :
            Object containing metadata
    """
    if metadata_obj['author'][0]['name'] == 'Your Name':
        warn("""Bundle creator name is not set and won't be added to the bundle metadata.
        Please consider setting your name in the carputils settings file.""")


def get_opencarp_git_hash():
    """
    Get the git hash of the openCARP version.

    Returns:
        the git hash as a string
    """
    f = os.popen(str(settings.execs.CARP) + ' -buildinfo')
    line_git_hash = ''
    for line in f:
        if 'GIT hash' in line:
            line_git_hash = line
            break
    f.close()
    try:
        git_hash = line_git_hash.split()[-1]
    except Exception as e:
        git_hash = ''
        print('WARNING: openCARP git hash could not be retrieved.')
    return git_hash


def get_latest_opencarp_version():
    """
    Get the latest version of openCARP.

    Returns:
        the latest version as a string, an empty string if the version could not be retrieved
    """
    f = os.popen('git ls-remote --tags --sort=-version:refname https://git.opencarp.org/openCARP/openCARP.git "v[0-9]*.*" | head -n 1 | sed \'s/.*\\(v[0-9].*\)/\\1/\'')
    latest_version = f.read().strip()
    f.close()
    if not latest_version.startswith('v'):
        latest_version = ''
        print('WARNING: Latest openCARP version could not be retrieved.')
    return latest_version