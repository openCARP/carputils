#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import re
import os
from collections import namedtuple

from carputils import settings


MESH_EXTENSIONS = ['.elem', '.pts', '.lon', '.belem', '.bpts', '.blon', '.surf']

# List of file parameters which are given as strings.
# They are manually added to the list of file parameters.
# The 'separator' field represents the separator used between file names.
StringFileParam = namedtuple('StringFileParam', 'name type separator')
STRING_FILEPARAMS = [StringFileParam('plug_sv_dumps', 'WFile', ':'),
                     StringFileParam('im_sv_dumps', 'WFile', ','),
                     StringFileParam('rt_lib', 'RFile', ':'),
                     StringFileParam('orthoname', 'RFile', ':')]


class _OCParam():
    """
    An object of this class represents an OpenCARP parameter.
    """

    def __init__(self, param_line):
        """
        A parameter object is initialized from the string used
        in the parameter list provided by OpenCARP.
        """
        # Raw string given in openCARP parameter list
        self._param_line = param_line
        s = param_line.split(maxsplit=1)
        # String given for the parameter name
        self._param_str = standardize_param_str(s[0])
        # String given for defining the type of the parameter
        self._param_type_str = s[1]
        # Type of the parameter and boolean telling if it is a list of parameters
        self._param_type, self._multiple_param = self.get_type_info()
        # File extension linked to the parameter (None if it doesn't exist)
        self._ext = self.get_ext()

    def get_type_info(self):
        """
        Get information about the type of the parameter

        Returns:
            Type of the parameter(s) and whether it is a list of
            parameters
        """
        is_list = re.match("'{(.*)x(.*)}'", self._param_type_str)
        if is_list:
            param_type = is_list.groups()[1].split()[0]
            multiple_param = True
        else:
            param_type = self._param_type_str
            multiple_param = False
        return param_type, multiple_param

    def get_ext(self):
        """
        Get the extension linked to the parameters if it exists.

        Returns:
            the extension of the parameter if it exists, None if not
        """
        f = os.popen(str(settings.execs.CARP) + ' +Help \"' + self._param_str + '\"')
        s = f.readlines()
        f.close()
        ext = None
        for line in s:
            l_split = line.split()
            if len(l_split) > 1:
                if l_split[0] == 'ext:':
                    ext = l_split[1]
        return ext


def get_OCparams():
    """
    Gets the list of openCARP parameters.

    Returns:
        The list of OpenCARP parameters
    """
    f = os.popen(str(settings.execs.CARP))
    s = f.read().split('Parameters:', 1)[1].splitlines()
    f.close()
    ret_list = []
    for line in s:
        if line:
            ret_list.append(_OCParam(line))
    return ret_list


def standardize_param_str(s):
    """
    Computes the standardized string representing a parameter

    Returns:
        The string representing the parameter
    """
    res = s.split()[0]
    if res[0] == "'" and res[-1] == "'":
        res = res[1:-1]
    if res[0] == '-':
        res = res[1:]
    res = re.sub("\[.*\]", "[0]", res)
    return res


def find_param(s, params):
    """
    Find the parameter corresponding to the string s in params, if it exists.

    Args:
        s : str
            string representing a parameter
    Returns:
        an instance of _OCParam corresponding to the parameter given in s,
        None if it doesn't exist.
    """
    s = standardize_param_str(s)
    res_param = None
    n = 0
    while res_param is None and n < len(params):
        if s == params[n]._param_str:
            res_param = params[n]
        n += 1
    return res_param


def filter_params(params, param_types):
    """
    Filters a list of parameters (instances of _OCParam), keeping
    only those having a type contained in param_types.

    Args:
        params : list of _OCParam
            A list of openCARP parameters
        param_types : list of str
            List of parameter types
    Returns:
        List of filtered _OCParam parameters
    """
    return [p for p in params if p._param_type in param_types]
