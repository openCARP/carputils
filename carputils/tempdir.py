#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Code to help with devising temporary directory names.
"""

import tempfile

from carputils import settings
from carputils.settings.exceptions import CARPUtilsSettingsError

def tempdir(suffix=''):
    """
    Generate a temporary directory with the specified suffix.
    """

    try:
        # Attempt to load regression temporary directory from settings file
        root = settings.config.REGRESSION_TEMP
    except CARPUtilsSettingsError:
        # No root temporary directory specified, use system dir
        tempdir = tempfile.mkdtemp(suffix=suffix)
    else:
        # Temp dir is specified, use it!
        tempdir = tempfile.mkdtemp(suffix=suffix, dir=root)
    
    return tempdir
