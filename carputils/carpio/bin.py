#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
"""
Provides a class for bench .bin file IO to/from python numpy arrays.
"""

import numpy as np
from carputils.carpio.filelike import FileLikeMixin

# save original file open
fopen = open

class BinFile(FileLikeMixin):
    """
    Bench .bin format file IO class.

    This class is little more than an elaborate wrapper for the numpy fromfile
    and tofile functions, but is included for completeness in the
    `carputils.carpio` module.
    """
    DTYPE = np.dtype('<d')

    def __init__(self, filename, mode='r'):
        """
        Args:
            filename (string): the filename to open
        Kwargs:
            mode (string): 'r' for read mode (default), 'w' for write mode
        """
        self._filename = filename
        self._mode = mode

    def close(self):
        """
        Close the file object.
        """
        pass

    def data(self):
        """
        Returns:
            numpy data array with the file contents
        """
        assert self._mode == 'r'
        return np.fromfile(self._filename, dtype=self.DTYPE)

    def write(self, data):
        """
        Write a numpy array to an IGB file.

        Args:
            data (numpy.ndarray): The array to be written to the IGB file
        """

        assert self._mode == 'w'

        np.array(data, dtype=self.DTYPE).tofile(self._filename)

def open(*args, **kwargs):
    """
    Open a bench .bin file.

    Convenience method to provide normal python style interface to create a
    file type object.

    Args:
        filename: the filename to open
    Kwargs:
        mode: 'r' for read mode (default), 'w' for write mode
    """
    return BinFile(*args, **kwargs)
