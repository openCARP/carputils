#!/usr/bin/python
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import os
import numpy as np
from carputils.carpio import txt, show, create_struct

def existence(mesh_name):
    """
    check if the files `mesh_name`.pts, `mesh_name`.elem, `mesh_name`.lon
    exist

    Parameters:
    -----------
        mesh_name : str
            base name of the openCARP mesh

    Returns:
    --------
        an object with boolean attributes `exists`, `points`
        `elements`, `fibers` indicating if the corresponding
        file exists
    """
    points = os.path.isfile(mesh_name+'.pts')
    elements = os.path.isfile(mesh_name+'.elem')
    fibers = os.path.isfile(mesh_name+'.lon')
    exists = points and elements and fibers

    return create_struct(exists= exists,
                         points= points,
                         elements = elements,
                         fibers = fibers)

def queryEdges(basemesh):

            """ QUERYEDGES
            query min/mean/max sizes of with given mesh (obtained from 'meshtool query edges')

            parameters:
            basemesh     (input) path to basename of the mesh

            returns:
                tuple of minimum, mean and maximum existing edge length
            """
            min_el  = -1
            mean_el = -1
            max_el  = -1

            queryFile = basemesh + '.mtquery'

            if not os.path.exists(queryFile):

                # compile meshtool command
                cmd = 'meshtool query edges -msh={} > {}'.format(basemesh, queryFile)
                # Note: no need to silence command
                os.system(cmd)

            with open(queryFile,'r') as fid:
                lines = fid.readlines()

                rows  = -1
                for line in lines:
                    rows += 1

                    if "Edge lengths" in line:
                        strOI   = lines[rows+2].strip().rstrip(')').split(',')

                        mean_el = float(strOI[1].split(':')[1])
                        min_el  = float(strOI[2].split(':')[1])
                        max_el  = float(strOI[3].split(':')[1])
                        break

            return min_el, mean_el, max_el

def write(msh):

        """
        Write the mesh stats (m_el and centroid) to msh+'.stats.out.txt

        params:
            msh     (input) Meshname
        outputs:
            msh+'.stats.out.txt'

        """

        stats_file=msh+'.stats.txt'

        if not os.path.exists(stats_file):
            show('Writing stats to file: {}'.format(stats_file),mode=2)
            msh_pts,_=txt.read(msh+'.pts')
            centroid = np.array(np.matrix(msh_pts).mean(0))[0]
            _,m_el,_ = queryEdges(msh)

            with open(stats_file,'w') as f:
                f.write(' '.join(([str(x) for x in centroid]))+'\n')
                f.write(str(m_el))

            f.close()

        return

def read(msh):

        """
        Read a mesh stats file (msh+'.stats.txt) for centroid and mean edge length.

        params
        msh:    (input) Meshname of a mesh with an already generated stats file.

        return:
            m_el, centroid
        """
        stats_file =msh+'.stats.txt'

        if not os.path.isfile(stats_file):
            raise IOError('Mesh stat file does not yet exist. Please write the file first.')

        with open(stats_file,'r') as f:
            centroid=np.array(f.readline().split(' '),dtype=float)
            m_el=float(f.readline())
        f.close()

        show('Centroid: {}'.format(centroid))
        show('Mean edge length: {}'.format(m_el))

        return m_el, centroid



