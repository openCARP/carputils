#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

import tables
import numpy as np
from carputils.carpio.filelike import FileLikeMixin

DEFAULT_TITLE = 'carputils generated file for limpetgui'

class LimpetGUIHDF5File(FileLikeMixin):
    """
    limpetgui HDF5 file IO class.

    Class derived from `carpio.filelike.FileLikeMixin`
    """

    def __init__(self, filename, mode='r', title=DEFAULT_TITLE):
        """
        Args:
            filename: (str) The filename to open.
        Kwargs:
            mode: (str) 'r' for read mode (default), 'w' for write mode
            title: (str) data title used in hdf5 file
        """
        self._h5file = tables.open_file(filename, mode, title)

    def close(self):
        """
        Close the file object.
        """
        self._h5file.close()

    def write(self, times=None, traces=[], named_traces={}):
        """
        Write data to the file.

        Kwargs:
            times: (array-like) Array of times for the traces
            traces: (list of array-like) List of unnamed traces to write to file
            named_traces: (dict of array-like) Dict of name-trace pairs to write to file
        """

        if times is not None:
            # Check data sizes match
            trace = np.asarray(traces)
            for items in named_traces.values():
                assert len(np.concatenate((trace, items))) == len(times), 'trace does not match times'

        group = self._h5file.create_group('/', 'protocol_1', 'protocol 1')
       
        if times is not None:
            # Write times
            self._h5file.create_array(group, 'time', times, 'Sample Times')
        
        # Auto name the traces
        for i, trace in enumerate(traces):
            self._h5file.create_array(group, 'trace_{:03d}'.format(i+1), trace,
                                      'Trace {}'.format(i+1))

        # Write named traces
        for name, trace in named_traces.items():
            self._h5file.create_array(group, name.replace(' ', '_'), trace, name)

        self._h5file.flush()

def open(*args, **kwargs):
    """
    Open a limpetgui HDF5 file.

    Convenience method to provide normal python style interface to create a
    file type object.

    Public Member Function inherited from `carpio.limpetguihdf5.LimpetGUIHDF5File.__init__()`


    Returns:
        `LimpetGUIHDF5File` object.
    """
    return LimpetGUIHDF5File(*args, **kwargs)
