#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the
# Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#

"""
Each module in this package defines a class for reading and writing a specific
data type. Some convenience methods are also provided for common file type
conversions.
"""

import os
import numpy as np
import time
from . import txt, igb, bin, csv, quantity

READERS = {'.igb':      igb.open,
           '.igb.gz':   igb.open,
           '.dynpt':    igb.open,
           '.dynpt.gz': igb.open,
           '.dat':      txt.open,
           '.dat.gz':   txt.open,
           '.vec':      txt.open,
           '.vec.gz':   txt.open,
           '.vtx':      txt.open,
           '.vtx.gz':   txt.open,
           '.bin':      bin.open,
           '.csv':      csv.open}

globalShowTime = False               # default display of time if showTime not passed to show()

def show(msg, mode=1, showTime=None):
    """
    Helper function to output status messages to the commandline.

    In case the standard terminal size can not be identified, the default message line length will be 80 characters.

    Args:
        msg: Message to send to the command line
    Kwargs:
        mode:   0 -- message will be framed with a header and footer e.g. "==="<br>
                1 -- message will be prepended with a hash tag
        showTime: append the time to the message
    """
    if mode < 0:
        return

    try:
        _, c = os.popen('stty size','r').read().split()
    except:
        c = 80
    finally:
        c = max(int(c), 1)

    if (globalShowTime if showTime is None else showTime):
        msg = str(msg) + "        @ {}".format(time.asctime())

    if mode == 0:
        print('#' + '='*(c-1))
        print('# ' + msg.upper())
        print('#' + '='*(c-1)+'\n')
    elif mode == 1:
        print('#' + '-'*(c-1))
        c_shift = 1 if len(msg) > (c+1) else int((c-len(msg))/2)
        print('#' + ' '*c_shift + msg)
        print('#' + '-'*(c-1)+'\n')
    else:
        print(msg)

def print_dict(dict_config ):

    config_str = ''

    width = max(len(key) for key in dict_config.keys())

    assert isinstance(dict_config,dict)

    for key, value in dict_config.items():
        if isinstance(dict_config[key],type):
            subdict = dict_config[key].__dict__
        else:
            subdict = dict_config[key]

        try:
                config_substr = ''
                subwidth = max(len(key) for key in subdict.keys())
                for subkey, subvalue in subdict.items():

                    if not subkey[0] == '_':
                        config_substr += '  {: <{}} :  {}\n'.format(subkey, subwidth, subvalue)

                config_str += '{: <{}}   \n{}\n'.format(key, width, config_substr)
        except:
                config_str += ' {: <{}} :  {}\n'.format(key, width, value)

    print(config_str)

def dat2adj(datfile, adjfile, grid='intra'):
    """
    Convert a .dat file to a nodal adjustment vector file.

    Args:
        datfile: (str) Path of .dat file to convert
        adjfile: (str) Path to write nodal adjustment vector file
    Kwargs:
        grid:    (str) String to enter in output file header as grid type
    """

    # Load dat file
    with txt.open(datfile) as fp:
        data = fp.data()

    # Add indicies
    data = np.column_stack((np.arange(len(data)), data))

    # Write nodal adjustment file
    with open(adjfile, 'w') as fp:

        # Write header
        fp.write('{}\n'.format(len(data)))
        fp.write('{}\n'.format(grid))

        # Write data
        np.savetxt(fp, data, fmt='%g')

def create_struct(**kwargs):
     """
     create structure with provided members

     Parameters:
     -----------
         kwargs : dict

     Returns:
     --------
         object of type `Struct` where the attributes
         are the keys of the provided `kwargs` holding
         the corresponding values
     """
     return type('Struct', (object,), kwargs)
