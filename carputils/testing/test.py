#!/usr/bin/env python3
#
# This file is part of openCARP
# (see https://www.openCARP.org).
#
# The openCARP project licenses this file to you under the 
# Apache License, Version 2.0 (the "License"); 
# you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
import os
import sys
import shutil
import datetime
import pickle
import base64
from contextlib import contextmanager, closing

from carputils import settings
from carputils.settings.exceptions import CARPUtilsSettingsError
from carputils import clean
from carputils.testing import tag
from carputils.testing.result import TestResult
from carputils.testing.check import Check
from carputils.testing.exception import CARPTestError, SerialisableError
from carputils import divertoutput
from carputils import stream
from carputils import format

from carputils.testing import check

IMPOSE_MAXIMUM_RUNTIME = True

@contextmanager
def temporary_wkdir(path):

    # Store current working directory
    orig_cwd = os.getcwd()

    try:
        # Change working directory to module location
        os.chdir(path)
        yield

    finally:
        # Change cwd back to previous state
        # try .. finally block ensures this always happens, even when an
        # Exception is raised
        os.chdir(orig_cwd)

@contextmanager
def overwrite_checks_disabled():
    # Disable overwrite checks
    clean.OVERWRITE_CHECK = False
    try:
        yield
    finally:
        # Re-enable overwrite checks
        clean.OVERWRITE_CHECK = True

@contextmanager
def catch_sys_exit():
    try:
        yield        
    except SystemExit as err:
        if sys.version_info.major > 2:
            if err.code is None:
                raise CARPTestError('sys.exit called').with_traceback(None)
            elif isinstance(err, int):
                tpl = 'sys.exit called with code {}'
                raise CARPTestError(tpl.format(err)).with_traceback(None)
            else:
                tpl = 'sys.exit called with message "{}"'
                raise CARPTestError(tpl.format(err)).with_traceback(None)
        else:
            from six import reraise as raise_
            backtrace = sys.exc_info()[2]
            if err.code is None:
                raise_(CARPTestError('sys.exit called'), None, backtrace)
            elif isinstance(err, int):
                tpl = 'sys.exit called with code {}'
                raise_(CARPTestError(tpl.format(err)), None, backtrace)
            else:
                tpl = 'sys.exit called with message "{}"'
                raise_(CARPTestError(tpl.format(err)), None, backtrace)

def _copy_reference(src, dest):
    """
    Function to copy reference solution files, taking account of gzipping.
    """

    # Determine the 'real' file paths to copy from and to
    #   realsrc is the discovered actual source file
    #   interdest is the path to be copied to (prior to possible g(un)zipping
    if src.endswith('.gz') and os.path.exists(src[:-3]):
        realsrc = src[:-3]
        interdest = dest[:-3]
    elif os.path.exists(src + '.gz'):
        realsrc = src + '.gz'
        interdest = dest + '.gz'
    else:
        realsrc = src
        interdest = dest

    # Attempt the copy
    shutil.copyfile(realsrc, interdest)

    # For logical tests
    srcbase = os.path.basename(realsrc)
    destbase = os.path.basename(dest)

    # Do any necessary conversions
    if srcbase + '.gz' == destbase:
        # Copy non-gzipped to gzipped
        divertoutput.call(['gzip', '-f', interdest])
    elif srcbase == destbase + '.gz':
        # Copy gzipped to non-gzipped
        divertoutput.call(['gunzip', '-f', interdest])

class Test(object):
    """
    Class defining a single regression test for openCARP.

    This class allows the simple definition of tests cases for openCARP simulations
    in the developer tests (and other python packages following the same
    layout).

    See the module docstring for detailed information on usage and the testing
    framework.

    Parameters
    ----------
    name : str
        A short unique name for the test
    run : callable
        The function executing the example
    argv : list, optional
        Command line arguments to run the example with
    description : str, optional
        Longer desctription of this test
    refdir : str, optional
        Override the name of the directory to save the reference solution in
    tags : list, optional
        List of tags from :mod:`carputils.testing.tag` to categorise this test
    """

    def __init__(self, name, run, argv=[], description=None, refdir=None,
                 tags=[]):

        self.name = str(name)
        self._run = run
        self._argv = list(argv)
        self.description = None if description is None else str(description)
        self._refdir = None if refdir is None else str(refdir)
        self._tags = list(tags)

        self.checks = []

        self.ref_generate = True

        # Register test with tags
        for tag in tags:
            tag.register(self)

    def serialise(self):
        data = {'name':         self.name,
                'run':          base64.b64encode(pickle.dumps(self._run)).decode('ascii'),
                'argv':         self._argv,
                'description':  self.description,
                'refdir':       self._refdir,
                'tags':         [str(tag) for tag in self._tags],
                'checks':       [chk.serialise() for chk in self.checks],
                'ref_generate': self.ref_generate}
        return data

    @classmethod
    def deserialise(cls, data):
        obj = cls(data['name'],
                  pickle.loads(base64.b64decode(data['run'])),
                  data['argv'],
                  data['description'],
                  data['refdir'],
                  [tag[name] for name in data['tags']])
        obj.checks = [Check.deserialise(chk) for chk in data['checks']]
        obj.ref_generate = data['ref_generate']
        return obj

    def add_filecmp_check(self, filename, metric, tol):
        """
        Add file comparison check.

        Stores a file comparison test for this test case. The test function can
        be whatever you want, but using the igb error functions in the testing
        module is highly recommended.

        Parameters
        ----------
        filename : str
            Name of file in sim output dir to compare
        metric : callable
            Function that takes two numpy arrays and computes an error metric
        tol : float or callable
            Tolerance on error evaluated by metric, or a function determining
            test sucess based on metric return val
        """

        chk = check.ReferenceComparisonCheck(filename, metric, tol)
        self.checks.append(chk)

    def add_analytic_check(self, filename, analytic, metric, tol):
        """
        Add analytic solution check.

        Stores a analytic solution test for this test case. The user must
        specify a comparison test which knows how to open the file and compare
        it against the analytic solution, and return an error.

        Parameters
        ----------
        filename : str
            Name of file in sim output dir to check
        analytic : callable
            Function returning the analytic solution when called with no
            arguments
        metric : callable
            Function that takes two numpy arrays and computes an error metric
        tol : float or callable
            Tolerance on error evaluated by metric, or a function determining
            test sucess based on metric return val
        """

        chk = check.AnalyticComparisonCheck(filename, analytic, metric, tol)
        self.checks.append(chk)

    def add_custom_check(self, check_function, *extraargs, **extrakwargs):
        """
        Add a custom check.

        Stores a custom check for this test case. Custom checks must simply be
        a function that returns a True/False value indicating whether the check
        passed. If an error occurs in running the check, raise an Exception
        with a descriptive message - this will be inserted into generated test
        reports.

        Parameters
        ----------
        check : callable
            Function returning True/False to indicate check success, or raising
            an Exception with appropriate message on check error. The function
            must take the simulation output directory as an argument.
        extraargs : any
            Additional arguments, such as a filename, to be passed to check
        """

        chk = check.CustomCheck(check_function, *extraargs, **extrakwargs)
        self.checks.append(chk)

    def disable_reference_generation(self):
        """
        Disables automatic reference generation for this test.
        """
        self.ref_generate = False

    @property
    def tags(self):
        return self._tags

    @property
    def module(self):
        """
        Returns the module that this test corresponds to.
        """
        return self._run.__module__

    def __eq__(self, other):
        return self.module == other.module and self.name == other.name

    def summary(self):
        """
        Generate a multi-line description of this test.
        """
        tpl = ('Module:\n' + '  {}\n'
               'Test:\n'   + '  {}')
        return tpl.format(self.module, self.name)

    def srcdir(self):
        """
        Returns the directory of the module this test corresponds to.
        """
        # Test's srcdir should correspond to run script
        mod = __import__(self.module, fromlist=[''])
        return os.path.dirname(mod.__file__)

    def refdir(self):
        """
        Generates the regression reference directory for this test.
        """
        root = settings.config.REGRESSION_REF
        subdir = self.name if self._refdir is None else self._refdir
        try:
            package, module = self.module.split('.', 1)
        except ValueError:
            # No '.' in module name: top level package
            return os.path.join(root, self.module, subdir)
        else:
            return os.path.join(root, package, module, subdir)

    def runtime_tag(self):
        """
        Return the tag for this test which indicates its runtime.
        """
        for tag in self.tags:
            if tag.runtime is not None:
                return tag
        return None

    def output_directory(self, root):
        """
        Determine the absolute path of the output directory for this test.

        Parameters
        ----------
        root : str
            The path of the parent directory

        Returns
        -------
        str
            The path of the output directory
        """
        folder = '{}_{}'.format(self.module, self.name)
        return os.path.abspath(os.path.join(root, folder))

    def execute(self, outdir, opstream=None):
        """
        Executes the simulation for this test.

        Parameters
        ----------
        outdir : str
            The simulation output directory to use
        opstream : stream, optional
            string stream to write sim output to
        """

        # Logfile to have same name as output directory
        logfile = outdir + '.log'

        # Prepare the command line
        argv = [str(v) for v in self._argv]

        # Add runtime from tags
        if IMPOSE_MAXIMUM_RUNTIME and '--runtime' not in argv:
            if self.runtime_tag() is not None:
                tag = self.runtime_tag()
                argv += ['--runtime', tag.runtime_string(factor=1.5)]

        # Merge provided output streams
        opstream = stream.merge(opstream)

        # Add log file to output streams
        fp = open(logfile, 'w')
        opstream = stream.merge([fp, opstream])

        # Prepare context
        if sys.version_info.major > 2:
            from contextlib import ExitStack
            context = ExitStack()
            resources = [closing(fp), # Close log file when done
                         # Run from source directory to ensure relative paths work
                         temporary_wkdir(self.srcdir()),
                         # Divert all python and sim output to stream
                         stream.divert_std(opstream),
                         # Raise exceptions when test does not exit correctly
                         divertoutput.subprocess_exceptions(),
                         # Raise exceptions when sys.exit called
                         catch_sys_exit()]
            for resource in resources:
                context.enter_context(resource)
        else:
            from contextlib import nested
            context = nested(closing(fp), # Close log file when done
                             # Run from source directory to ensure relative paths work
                             temporary_wkdir(self.srcdir()),
                             # Divert all python and sim output to stream
                             stream.divert_std(opstream),
                             # Raise exceptions when test does not exit correctly
                             divertoutput.subprocess_exceptions(),
                             # Raise exceptions when sys.exit called
                             catch_sys_exit())
        with context:
            # Write out a header before simulation, including the
            # command line of the test
            opstream.write('\n\n')
            opstream.write('#'*70)
            tpl = '\n\nBEGIN TEST "{}" IN MODULE "{}"\n\n'
            opstream.write(tpl.format(self.name, self.module))

            tpl = 'Working directory: {}\n\n'
            opstream.write(tpl.format(self.srcdir()))

            opstream.write('Run command:\n\n')

            # Determine the module filename
            script_name = self._run.__module__.split('.')[-1]
            script_name = './{}.py'.format(script_name)

            # Write the test command line
            cmd = [script_name] + argv
            opstream.write(format.command_line(cmd))
            opstream.write('\n\n')

            # Call the script's run function to actually do the sim
            self._run(argv, outdir=outdir)

            # Write footer following test output
            tpl = '\nEND TEST "{}" IN MODULE "{}"\n\n'
            opstream.write(tpl.format(self.name, self.module))
            opstream.write('#'*70)
            opstream.write('\n\n')

    def execute_and_check(self, outdir, opstream=None):
        """
        Run the simulation and return all test results.

        Parameters
        ----------
        outdir : str
            The simulation output directory to use
        opstream : stream, optional
            string stream to write sim output to

        Returns
        -------
        carputils.testing.result.TestResult
            The test result
        """

        # Prepare results
        now = datetime.datetime.now()
        results = TestResult(self, now)

        # Don't both running if no checks are defined
        if len(self.checks) == 0:
            err = CARPTestError('no checks defined for this test')
            results.exception = err
            return results

        # Run the simulation
        start = datetime.datetime.now()
        try:
            self.execute(outdir, opstream)
        except Exception as err:
            backtrace = sys.exc_info()[2]
            results.error = SerialisableError(err, backtrace)
            return results
        finally:
            # Always set the execution time and openCARP revision
            end = datetime.datetime.now()
            results.execution_time = end - start
            try:
                results.revision = settings.revision
                results.dep_revisions = settings.dependency_revisions
            except CARPUtilsSettingsError:
                pass

        self.perform_checks(outdir, results)

        return results

    def perform_checks(self, outdir, result=None):
        """
        Perform this test's checks on a preexisting simulation output.

        Parameters
        ----------
        outdir : str
            The path of the simulation output directory
        result : TestResult, optional
            The test result to add the check results to
        """

        # Generate a new result if none was given
        result = result or TestResult(self)

        # Get absolute path, just to be safe!
        outdir = os.path.abspath(outdir)
        refdir = self.refdir()

        # Perform checks from source directory to ensure relative paths work
        with temporary_wkdir(self.srcdir()):
            for check in self.checks:
                result.add_result(check(testdir=outdir, refdir=refdir))

        return result

    def copy_reference(self, outdir):
        """
        Copy all required reference files from the output to the reference dir.

        Parameters
        ----------
        outdir : str
            The simulation output directory to use as the reference
        """

        # Get absolute path, just to be safe!
        outdir = os.path.abspath(outdir)
        refdir = self.refdir()

        files = []

        # Perform copies from source directory to ensure relative paths work
        with temporary_wkdir(self.srcdir()):

            for chk in self.checks:

                # Only need files for ReferenceComparisonChecks
                if not isinstance(chk, check.ReferenceComparisonCheck):
                    continue

                src = os.path.join(outdir, chk.filename)
                dest = os.path.join(refdir, chk.filename)

                # Make sure dest directory exists
                destdir = os.path.dirname(dest)
                if not os.path.exists(destdir):
                    os.makedirs(destdir)

                # Copy the file
                _copy_reference(src, dest)

                files.append(dest)

        return files
